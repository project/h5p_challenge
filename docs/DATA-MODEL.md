# Data model

## General description of functionality

H5P Challenge allows anyone to create challenges based on existing H5P content. Both anonymous and authenticated users
can create Challenges. The main difference is that Challenges created by anonymous users are removed from the system
once the Challenge has ended and 36 hours has passed since then. Creator will receive two email messages per each
challenge created. The initial one will have Challenge code and results URL. The second message will have information
about the challenge and all the results in CSV format. Challenge results view will try to detect complex H5P content
types and display information about contained questions. The solution is far from perfect and might not work with in all
the cases, though it should have all the default ones covered (Column and Course Presentation a few examples of those
kinds of templates).

## Data structures

* **h5p_challenge** - Stores data for all currently existing Challenges. Ended anonymous Challenges are stored in the
database for 36 more hours, after which they will be removed.
  * `uuid` - Challenge unique identifier based on UUID v4.
  * `content_id` - A unique identifier for H5P Content entity.
  * `title` - Title of the Challenge.
  * `email` - Email address of the Challenge creator.
  * `started` - Challenge start time.
  * `finished` - Challenge end time.
  * `code` - Challenge unique code used by participants (removed once the Challenge has ended).
  * `results_sent` - Mark if results notification has already been sent.
  * `langcode` - Language code for the creator session. Used to determine language for sent email messages.
  * `user_id` - Unique user identifier or NULL for anonymous users.
  * `data` - Serialized data. Part of the data depends on H5P content type being used and allows the UI to display
additional information for questions contained within complex data types.
* **h5p_challenge_points** - Stores Challenge results for a single participant.
  * `challenge_uuid` - Challenge unique identifier.
  * `player_uuid` - Player unique identifier based on UUID v4.
  * `name` - Player name.
  * `started` - When current participant started the interaction.
  * `finished` - When current participant submitted the result.
  * `points` - The participants score.
  * `max_points` - The maximum score for this Challenge.
  * `tries_count` - The number of times Challenge score was submitted.
  * `user_id` - Unique user identifier or NULL for anonymous users.
  * `data` - Serialized data.
* **h5p_challenge_statistics** - Stores statistical information about finished challenges. This data is kept for both
removed and kept challenges.
  * `content_id` - The unique identifier for H5P Content.
  * `started` - Challenge start time.
  * `finished` - Challenge end time.
  * `player_count` - Count of unique participants that took part in the Challenge.

![Database tables](images/data_model.png "Challenge specific tables")

### Serialised data examples

#### Challenge
Challenge serialised data holds the initial content title and information about questions contained within complex 
template structures:

* **content** - Main structural data that has information about H5P Content title and contained questions.
  * `title` - H5P Content title if of exists, with default empty textual value.
  * `questions` - Detected sub questions for known complex data types. Each such question has `id`, `type` and `title`
keys. Those hold `subContentId` from the H5P data structure, library name and version, title of the question. Module code has
detailed information on supported types and the general logic. Default value is an empty array.

```php
array (
  'content' => 
  array (
    'title' => 'Cement',
    'questions' => 
    array (
      '318512da-b2ea-4079-9321-8d90646694ef' => 
      array (
        'id' => '318512da-b2ea-4079-9321-8d90646694ef',
        'type' => 'H5P.QuestionSet 1.17',
        'title' => 'Cement: comprehension',
      ),
      '40f742b2-948c-4203-8022-9cf24deab564' => 
      array (
        'id' => '40f742b2-948c-4203-8022-9cf24deab564',
        'type' => 'H5P.DragText 1.8',
        'title' => 'Drag Text',
      ),
      '22a4b9ba-f81f-4622-a224-4ab9f15dc902' => 
      array (
        'id' => '22a4b9ba-f81f-4622-a224-4ab9f15dc902',
        'type' => 'H5P.DragText 1.8',
        'title' => 'Mix design',
      ),
    ),
  ),
```

#### Participant
Challenge response serialised data holds participant unique identifier, some basic information about the challenge
itself, optional data about duration and question answers:

* **uuid** - Unique participant identifier.
* **challenge** - Basic challenge data.
  * `title` - Challenge title.
  * `started` - Challenge start time.
  * `finished` - Challenge end time.
  * `duration` - Time taken to complete the tasks (optional).
  * `answers` - Answers given (optional). Extracted on the H5P side and possibly holds the data for each of the detected
sub-questions that were previously stored as an additional data for the Challenge itself.

```php
array (
  'duration' => 'PT63.97S',
  'answers' => 
  array (
    '22a4b9ba-f81f-4622-a224-4ab9f15dc902' => 
    array (
      'id' => '22a4b9ba-f81f-4622-a224-4ab9f15dc902',
      'score' => '0',
      'maxScore' => '7',
      'duration' => 'PT29.38S',
      'attempts' => '1',
    ),
    '40f742b2-948c-4203-8022-9cf24deab564' => 
    array (
      'id' => '40f742b2-948c-4203-8022-9cf24deab564',
      'score' => '1',
      'maxScore' => '3',
      'duration' => 'PT38.74S',
      'attempts' => '1',
    ),
    '54ded0ed-02d2-46ac-90c3-ef5aa4cfc0a8' => 
    array (
      'id' => '54ded0ed-02d2-46ac-90c3-ef5aa4cfc0a8',
      'score' => '4',
      'maxScore' => '4',
      'duration' => 'PT45.27S',
      'attempts' => '1',
    ),
    '7f415861-3077-4bf9-9039-136fd60feb97' => 
    array (
      'id' => '7f415861-3077-4bf9-9039-136fd60feb97',
      'score' => '1',
      'maxScore' => '1',
      'duration' => 'PT8.73S',
      'attempts' => '1',
    ),
    '318512da-b2ea-4079-9321-8d90646694ef' => 
    array (
      'id' => '318512da-b2ea-4079-9321-8d90646694ef',
      'score' => '5',
      'maxScore' => '5',
      'duration' => '',
      'attempts' => '1',
    ),
  ),
```