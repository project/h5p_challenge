<?php

declare(strict_types=1);

namespace Drupal\h5p_challenge;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;
use Symfony\Component\Routing\Route;

/**
 * Converts H5PChallenge UUID to the challenge instance
 *
 * @DCG
 * To use this converter specify parameter type in a relevant route as follows:
 * @code
 * h5p_challenge.h5p_challenge_example_route:
 *   path: example/{challenge}
 *   defaults:
 *     _controller: '\Drupal\h5p_challenge\Controller\H5PChallengeController::example'
 *   requirements:
 *     _access: 'TRUE'
 *   options:
 *     parameters:
 *       challenge:
 *        type: h5p_challenge
 * @endcode
 */
final class H5PChallengeParamConverter implements ParamConverterInterface {

  /**
   * @var H5PChallengeServiceInterface H5P Challenge service
   */
  private H5PChallengeServiceInterface $h5pChallengeService;

  /**
   * Constructs a H5pChallengeParamConverter object.
   */
  public function __construct(H5PChallengeServiceInterface $h5p_challenge_service) {
    $this->h5pChallengeService = $h5p_challenge_service;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults): ?H5PChallenge {
    return $this->h5pChallengeService
      ->getChallengeByUuid($value) ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    return isset($definition['type']) && $definition['type'] === 'h5p_challenge';
  }

}
