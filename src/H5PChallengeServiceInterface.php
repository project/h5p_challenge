<?php

namespace Drupal\h5p_challenge;

use Drupal\Component\Render\MarkupInterface;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;

interface H5PChallengeServiceInterface {

  /**
   * Returns reCaptcha site key setting value
   *
   * @return string|null
   *   Site key setting
   */
  public function getRecaptchaSiteKey(): ?string;

  /**
   * Returns reCaptcha secret key setting value
   *
   * @return string|null
   *   Secret key setting
   */
  public function getRecaptchaSecretKey(): ?string;

  /**
   * Returns email address to be used when sending notifications
   *
   * @return string|null
   *   Email address setting
   */
  public function getEmailFromAddress(): ?string;

  /**
   * Generates a string of random numbers from 0 to 9 for a given length
   *
   * @param  int    $length
   *   Length of the result string
   * @return string
   *   String of random numbers with given length
   */
  public function generateRandomNumber(int $length): string;

  /**
   * Makes sure that code is unique for certain content
   *
   * @param  string  $code
   *   Code to check
   * @param  int     $contentId
   *   Content identifier
   * @return boolean
   *   Code is not yet being used
   */
  public function isUniqueChallengeCode(string $code, int $contentId): bool;

  /**
   * Generates unique challenge code for certain content
   *
   * @param  int    $contentId
   *   Content identifier
   * @return string
   *   Unique numeric code
   */
  public function generateUniqueChallengeCode(int $contentId): string;

  /**
   * Determines if content with provided identifier does exist
   *
   * @param  int    $contentId
   *   Content identifier
   * @return boolean
   *   Content exists or not
   */
  public function checkIfContentExists(int $contentId): bool;

  /**
   * Loads challenge by content id and code
   * @param  int    $contentId
   *   Content identifier the challenge is created for
   * @param  string $code
   *   Challenge code that is unique in context of certain content
   * @return H5PChallenge|null
   *   Challenge instance or empty result
   */
  public function getChallengeByContentIdAndCode(int $contentId, string $code): ?H5PChallenge;

  /**
   * Loads challenge points by player unique identifier
   *
   * @param  string $uuid
   *   Player unique identifier
   * @return object|null
   *   Object or empty result
   */
  public function getChallengePointsByUuid(string $uuid): ?object;

  /**
   * Loads challenge by unique identifier
   *
   * @param  string $uuid
   *   Challenge unique identifier
   * @return H5PChallenge|null
   *   Challenge instance or empty result
   */
  public function getChallengeByUuid(string $uuid): ?H5PChallenge;

  /**
   * Returns CSV results for a challenge
   *
   * @param H5PChallenge $challenge
   *   Challenge instance
   * @return string
   *   Results CSV
   */
  public function getChallengeResultsCsv(H5PChallenge $challenge): string;

  /**
   * Determines challenges with unsent results, sends notifications and marks
   * challenge with a specific flag to indicate it.
   */
  public function notificationsCron(): void;

  /**
   * Removes challenges and their results in case more than 36 hours has passed
   * since challenge end time.
   */
  public function cleanupCron(): void;

  /**
   * Challenge localization
   *
   * @return array<string>
   *   Challenge specific localizations
   */
  public function getLocalization(): array;

  /**
   * Converts time taken to minutes.
   *
   * @param  int    $time_taken
   *   Time taken in seconds
   * @return string
   *   Time taken or empty string
   */
  public function userFriendlyTimeTaken(int $time_taken): string;

  /**
   * Returns mastery level text.
   *
   * @param  int    $points
   *   Points received
   * @param  int    $max_points
   *   Maximum possible points
   * @param  int    $attempts
   *   Number of attempts
   *
   * @return string
   *   Mastery level text
   */
  public function masteryLevelText(int $points, int $max_points, int $attempts): string;

  /**
   * Returns JS settings used by challenges.
   *
   * @return array{recaptchaSiteKey: string|null, l10n: array<string>, language: string, user?: array{display_name: MarkupInterface|string, mail: string|null}}
   *   Settings for JS context
   */
  public function getJsSettings(): array;

}
