<?php

namespace Drupal\h5p_challenge\Controller;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Mail\MailFormatHelper;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Uuid\Php as Uuid;
use Drupal\h5p_challenge\H5PChallengeServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Url;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\h5p\Entity\H5PContent;
use Drupal\h5p_challenge\FetchClass\H5PChallengePoints;
use Drupal\user\Entity\User;

/**
 * Class H5PChallengeController.
 */
final class H5PChallengeController extends ControllerBase {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var Connection
   */
  protected Connection $database;

  /**
   * Uuid service
   *
   * @var Uuid
   */
  protected Uuid $uuid;

  /**
   * Date formatter
   *
   * @var DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Mail manager
   *
   * @var MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * Challenge service
   *
   * @var H5PChallengeServiceInterface
   */
  protected H5PChallengeServiceInterface $challengeService;

  /**
   * Language manager
   *
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Current user
   *
   * @var AccountProxyInterface $currentUser
   */
  protected $currentUser;

  /**
   * Email validator
   *
   * @var EmailValidatorInterface
   */
  protected EmailValidatorInterface $emailValidator;

  /**
   * HTTP Client
   *
   * @var Client
   */
  protected Client $httpClient;

  /**
   * H5P Challenge logger
   *
   * @var LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a new H5PChallengeController object.
   */
  public function __construct(Connection $database, Uuid $uuid, H5PChallengeServiceInterface $challengeService, DateFormatterInterface $dateFormatter, MailManagerInterface $mailManager, LanguageManagerInterface $languageManager, AccountProxyInterface $currentUser, EmailValidatorInterface $emailValidator, Client $httpClient, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->database = $database;
    $this->uuid = $uuid;
    $this->challengeService = $challengeService;
    $this->dateFormatter = $dateFormatter;
    $this->mailManager = $mailManager;
    $this->languageManager = $languageManager;
    $this->currentUser = $currentUser;
    $this->emailValidator = $emailValidator;
    $this->httpClient = $httpClient;
    $this->logger = $loggerChannelFactory->get('h5p_challenge');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('uuid'),
      $container->get('h5p.challenge.default'),
      $container->get('date.formatter'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'),
      $container->get('current_user'),
      $container->get('email.validator'),
      $container->get('http_client'),
      $container->get('logger.factory')
    );
  }

  /**
   * Start new challenge
   *
   * @return JsonResponse
   *   JSON response with possible additional data or message
   * @throws Exception|GuzzleException
   */
  public function createNew(Request $request): JsonResponse
  {
    $content_id = filter_input(INPUT_POST, 'contentId', FILTER_VALIDATE_INT);
    $title = filter_input(INPUT_POST, 'title');
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $duration = filter_input(INPUT_POST, 'duration', FILTER_VALIDATE_INT);
    $captcha = filter_input(INPUT_POST, 'g-recaptcha-response');
    $token = filter_input(INPUT_GET, 'token');

    if (! ($content_id && $title && $email !== NULL && $email !== FALSE && $duration !== NULL && $duration !== FALSE && $captcha && $token)) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Wrong usage'),
      ]);
    }

    /*if (! \H5PCore::validToken('result', $token)) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Invalid security token'),
      ]);
    }*/

    if (! $this->emailValidator->isValid($email)) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Provided email address is invalid'),
      ]);
    }

    if ($duration < 1 || $duration > 168) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Provided duration is invalid'),
      ]);
    }

    if (!$this->challengeService->checkIfContentExists($content_id)) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Content does not exist'),
      ]);
    }

    try {
      $captchaResponse = $this->httpClient->post(H5P_CHALLENGE_RECAPTCHA_VERIFY_ENDPOINT, [
        'form_params' => [
          'secret' => $this->challengeService->getRecaptchaSecretKey(),
          'response' => $captcha,
          'remoteip' => $request->getClientIp(),
        ],
      ]);
    } catch (Exception $e) {
      $this->logger->error($e->getMessage());

      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Captcha service responded with an error'),
      ]);
    }

    if ($captchaResponse->getStatusCode() !== 200 || $captchaResponse->getReasonPhrase() !== 'OK') {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Captcha service responded with an error'),
      ]);
    }

    $parsedResponse = json_decode($captchaResponse->getBody(), TRUE);
    if ($parsedResponse['success'] !== TRUE) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Wrong or unsuitable captcha'),
      ]);
    }

    $started = time();
    $finished = $started + ($duration * 60 * 60);
    $code = $this->challengeService->generateUniqueChallengeCode($content_id);
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $uuid = $this->uuid->generate();
    $url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_results', [
      'challenge' => $uuid,
    ], [
      'absolute' => TRUE,
    ])->toString();

    // TODO Handle failures?
    $this->database->insert('h5p_challenge')->fields([
      'uuid' => $uuid,
      'content_id' => $content_id,
      'title' => $title,
      'email' => $this->currentUser->isAuthenticated() ? $this->currentUser->getEmail() : $email,
      'started' => $started,
      'finished' => $finished,
      'code' => $code,
      'langcode' => $langcode,
      'user_id' => $this->currentUser->isAuthenticated() ? $this->currentUser->id() : NULL,
      'data' => serialize([
        'content' => [
          'title' => H5PChallenge::getContentTitleById($content_id),
          'questions' => H5PChallenge::getContentQuestions(H5PContent::load($content_id)),
        ],
      ]),
    ])->execute();

    if ($this->currentUser->isAuthenticated()) {
      $message = t("You have created a new challenge: @title.\nThe title of a resource for this challenge is: @ctitle.\n\nThe challenge will be running from @started to @finished (the duration would be @hours hours).\n\nChallenge code to participate is: @code\n\nYou can track results at: @results\nAnother notification containing results would be sent to you soon after the challenge is over.\nThe results page will never expire and you can always access it from the list of your challenges.\n\n\nBest regards,\nChallenge team.", [
        '@title' => $title,
        '@ctitle' => H5PChallenge::getContentTitleById($content_id),
        '@started' => $this->dateFormatter->format($started, 'long'),
        '@finished' => $this->dateFormatter->format($finished, 'long'),
        '@hours' => $duration,
        '@code' => $code,
        '@results' => $url,
      ]);
    } else {
      $message = t("You have created a new challenge: @title.\nThe title of a resource for this challenge is: @ctitle.\n\nThe challenge will be running from @started to @finished (the duration would be @hours hours).\n\nChallenge code to participate is: @code\n\nYou can track results at: @results\nAnother notification containing results would be sent to you soon after the challenge is over.\nThe results link would still be active for at least 36 hours after the challenge ends.\n\n\nBest regards,\nChallenge team.", [
        '@title' => $title,
        '@ctitle' => H5PChallenge::getContentTitleById($content_id),
        '@started' => $this->dateFormatter->format($started, 'long'),
        '@finished' => $this->dateFormatter->format($finished, 'long'),
        '@hours' => $duration,
        '@code' => $code,
        '@results' => $url,
      ]);
    }
    $params = [
      'body' => (string) $message,
      'plaintext' => MailFormatHelper::wrapMail((string) $message),
    ];
    $this->mailManager->mail('h5p_challenge', 'new_challenge_created', $email, $langcode, $params, NULL, TRUE);

    return new JsonResponse([
      'success' => TRUE,
      'code' => $code,
      'url' => $url,
    ]);
  }

  /**
   * Join existing challenge
   *
   * @return JsonResponse
   *   JSON response with possible additional data or message
   * @throws Exception
   */
  public function startPlaying(): JsonResponse {
    $content_id = filter_input(INPUT_POST, 'contentId', FILTER_VALIDATE_INT);
    $name = filter_input(INPUT_POST, 'name');
    $code = filter_input(INPUT_POST, 'code');
    $token = filter_input(INPUT_GET, 'token');

    if (! ($content_id && $name && $code && $token)) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Wrong usage'),
      ]);
    }

    /*if (! \H5PCore::validToken('result', $token)) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Invalid security token'),
      ]);
    }*/

    $challenge = $this->challengeService->getChallengeByContentIdAndCode($content_id, $code);

    if (!$challenge) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('No challenge could be found for provided code'),
      ]);
    } else if (!$challenge->isActive()) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Challenge is no longer active'),
      ]);
    }

    $uuid = $this->uuid->generate();

    $this->database->insert('h5p_challenge_points')->fields([
      'challenge_uuid' => $challenge->uuid,
      'player_uuid' => $uuid,
      'name' => $this->currentUser->isAuthenticated() ? $this->currentUser->getDisplayName() : $name,
      'started' => time(),
      'user_id' => $this->currentUser->isAuthenticated() ? $this->currentUser->id() : NULL,
    ])->execute();

    $data = [
      'uuid' => $uuid,
      'challenge' => [
        'title' => $challenge->title,
        'started' => $challenge->started,
        'finished' => $challenge->finished,
      ],
    ];

    user_cookie_save(['h5p-challenge-for-' . $content_id => json_encode($data),]);

    return new JsonResponse(array_merge($data, [
      'success' => TRUE,
    ]));
  }

  /**
   * Store results for challenge participant
   *
   * @return JsonResponse
   *   JSON response with possible additional data or message
   * @throws Exception
   */
  public function setFinished(): JsonResponse {
    // TODO NB! The content_id is not needed and could be taken from challenge data that is loaded using points record
    $content_id = filter_input(INPUT_POST, 'contentId', FILTER_VALIDATE_INT);
    $uuid = filter_input(INPUT_POST, 'uuid');
    $score = filter_input(INPUT_POST, 'score', FILTER_VALIDATE_INT);
    $max_score = filter_input(INPUT_POST, 'maxScore', FILTER_VALIDATE_INT);
    $token = filter_input(INPUT_GET, 'token');
    $duration = filter_input(INPUT_POST, 'duration');
    $question_answers = filter_input(INPUT_POST, 'questionAnswers', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    if (! ($content_id && $uuid && $score !== NULL && $score !== FALSE && $max_score !== NULL && $max_score !== FALSE && $token)) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Wrong usage'),
      ]);
    }

    /*if (! \H5PCore::validToken('result', $token)) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Invalid security token'),
      ]);
    }*/

    $points = $this->challengeService->getChallengePointsByUuid($uuid);

    if (!$points) {
      // TODO See if it would make sense to provide a message
      return new JsonResponse([
        'success' => FALSE,
      ]);
    }

    $challenge = $this->challengeService->getChallengeByUuid($points->challenge_uuid);

    if (!$challenge) {
      // TODO See if it would make sense to provide a message
      return new JsonResponse([
        'success' => FALSE,
      ]);
    } else if (!$challenge->isActive()) {
      return new JsonResponse([
        'success' => FALSE,
        'message' => $this->t('Challenge is no longer active'),
      ]);
    }

    $finished_time = time();

    $this->database->update('h5p_challenge_points')
      ->fields([
        'finished' => $finished_time,
        'points' => $score,
        'max_points' => $max_score,
        'data' => serialize([
          'duration' => $duration,
          'answers' => $question_answers,
        ]),
      ])
      ->expression('tries_count', 'tries_count + :amount', [':amount' => 1,])
      ->condition('player_uuid', $uuid)
      ->execute();

    // Update cookie contents to indicate that score has been stored
    $cname = 'Drupal_visitor_h5p-challenge-for-' . $content_id;
    if (isset($_COOKIE[$cname])) {
      $data = json_decode($_COOKIE[$cname], TRUE);
      $data['finished'] = [
        'finished' => $finished_time,
        'score' => $score,
        'maxScore' => $max_score,
      ];
      user_cookie_save(['h5p-challenge-for-' . $content_id => json_encode($data),]);
    }

    return new JsonResponse([
      'success' => TRUE,
    ]);
  }

  /**
   * Generates mastery level markup.
   *
   * @param int $points
   *   Points received
   * @param int $max_points
   *   Maximum possible points
   * @param int $attempts
   *   Number of tries
   *
   * @return MarkupInterface Markup with colored round
   *   Markup with colored round
   */
  private function masteryLevelMarkup(int $points, int $max_points, int $attempts): MarkupInterface {
    $class = 'mastery-level';
    $text = $this->challengeService->masteryLevelText($points, $max_points, $attempts);

    if ($max_points !== 0 && $attempts !== 0) {
      $score_percent = $points / $max_points * 100;

      if ($score_percent === 100 && $attempts === 1) {
        $class .= ' green';
      } else if ($score_percent > 51 && $attempts <= 2 ) {
        $class .= ' orange';
      } else {
        // $score_percent < 50 || $tries_count > 2
        $class .= ' red';
      }
    }

    return Markup::create('<span data-tippy-content="' . $text . '" class="' . $class . '"></span>');
  }

  /**
   * Show challenge results page.
   *
   * @return array
   *   Page structure
   * @throws Exception
   */
  public function results(H5PChallenge $challenge): array
  {
    $response = [];
    $response['#title'] = $this->t('Challenge results: @title', [
      '@title' => $challenge->title,
    ]);

    if ($this->currentUser->id() === $challenge->user_id && $challenge->code) {
      $response['challenge_code'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('Code: :code', [
          ':code' => $challenge->code,
        ]),
        '#attributes' => [
          'class' => ['challenge-code'],
        ],
      ];
    }

    $header = [
      '#',
      $this->t('Name'),
      $this->t('Mastery level'),
      $this->t('Total score'),
    ];

    $questions = $challenge->getQuestions();
    $question_identifiers = $challenge->getQuestionIds();
    if (count($questions) > 0) {
      foreach (array_values($questions) as $index => $question) {
        $header[] = Markup::create('<span data-tippy-content="' . $question['title'] . '" class="question cursor-pointer">' . $this->t('Q@index', ['@index' => $index + 1]) . '</span>');
      }
    }

    $header = array_merge($header, [
      Markup::create('<span data-tippy-content="' . $this->t('Challenge joining time') . '" class="cursor-pointer">' . $this->t('Started') . '</span>'),
      Markup::create('<span data-tippy-content="' . $this->t('Latest attempt submission time') . '" class="cursor-pointer">' . $this->t('Finished') . '</span>'),
      Markup::create('<span data-tippy-content="' . $this->t('Timespan between joining the challenge and latest attempt submission time') . '" class="cursor-pointer">' . $this->t('Time taken') . '</span>'),
      $this->t('Attempts'),
    ]);

    $rows = [];

    $result = $this->database->select('h5p_challenge_points', 'cp', [
      'fetch' => H5PChallengePoints::class,
    ])
      ->fields('cp')
      ->condition('cp.challenge_uuid', $challenge->uuid)
      ->orderBy('cp.started', 'ASC')
      ->execute();

    if ($result) {
      $no = 1;
      while($single = $result->fetch()) {
        /** @var H5PChallengePoints $single */
        $tmp = [
          'data' => [
            'no' => $no . '.',
            'name' => $single->name,
            'mastery_level' => [
              'data' => $this->masteryLevelMarkup((int)$single->points, (int)$single->max_points, (int)$single->tries_count),
              'class' => 'mastery-level'
            ],
            'total_score' => ($single->points ?? 0) . '/' . ($single->max_points ?? 0),
          ],
        ];

        if (!$single->hasAllQuestionsAnswered($question_identifiers)) {
          $tmp['data']['total_score'] = Markup::create('<span data-tippy-content="' . $this->t('Some answers are missing!') . '" class="questions-with-missing-answers">' . $tmp['data']['total_score'] . '</span>');
        } else if ($single->max_points) {
          $completion = round(($single->points ?? 0) / $single->max_points * 100);

          $tmp['data']['total_score'] = Markup::create('<span data-tippy-content="' . $completion . '%" class="cursor-pointer">' . $tmp['data']['total_score'] . '</span>');
        }

        if (count($questions) > 0) {
          $answers = $single->getAnswers();

          foreach ($questions as $question) {
            $tmp['data'][$question['id']] = '';

            if (isset($answers[$question['id']]) && $answers[$question['id']]['maxScore']) {
              $completion = round(($answers[$question['id']]['score'] ?? 0) / $answers[$question['id']]['maxScore'] * 100);

              $tmp['data'][$question['id']] = Markup::create('<span data-tippy-content="' . $completion . '%" class="cursor-pointer">' . $answers[$question['id']]['score'] . '/' . $answers[$question['id']]['maxScore'] . '</span>');
            }
          }
        }

        $tmp['data']['started'] = $this->dateFormatter->format($single->started, 'long');
        $tmp['data']['finished'] = $single->finished ? $this->dateFormatter->format($single->finished, 'long') : '';
        $tmp['data']['time_taken'] = $this->challengeService->userFriendlyTimeTaken($single->finished ? ($single->finished - $single->started) : 0);
        $tmp['data']['attempts'] = $single->tries_count;

        $rows[] = $tmp;
        $no++;
      }
    }

    $response['actions'] = [
      '#type' => 'dropbutton',
      '#dropbutton_type' => 'small',
      '#links' => [
        'download_results_csv' => [
          'title' => $this
            ->t('Download results as CSV'),
          'url' => Url::fromRoute('h5p_challenge.h5p_challenge_controller_results_csv', [
            'challenge' => $challenge->uuid,
          ], [
            'absolute' => TRUE,
          ]),
        ],
      ],
    ];

    $entity = $challenge->getContentEntity();

    if ($entity) {
      $response['actions']['#links']['target_entity'] = [
        'title' => $this
          ->t('Target content'),
        'url' => Url::fromUri('internal:/h5p/' . $entity->id() . '/embed', [
          'absolute' => TRUE,
        ]),
        'attributes' => [
          'target' => '_blank',
        ],
      ];
    }

    if ($challenge->belongsToUser() && $this->currentUser->id() === $challenge->user_id) {
      if ($challenge->isActive()) {
        $response['actions']['#links']['end'] = [
          'title' => $this
            ->t('End challenge'),
          'url' => Url::fromRoute('h5p_challenge.h5p_challenge_end_form', [
            'challenge' => $challenge->uuid,
          ], [
            'absolute' => TRUE,
          ]),
        ];
      }

      $response['actions']['#links']['delete'] = [
        'title' => $this
          ->t('Delete'),
        'url' => Url::fromRoute('h5p_challenge.h5p_challenge_delete_form', [
          'challenge' => $challenge->uuid,
        ], [
          'absolute' => TRUE,
        ]),
      ];
    }

    $response['results-table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#empty' => $this->t('No participants found'),
      '#attached' => [
        'library' => [
          'h5p_challenge/results',
        ],
      ],
      '#attributes' => [
        'class' => [
          'challenge-results',
        ],
      ],
    ];

    return $response;
  }

  /**
   * Serve challenge results to be downloaded as CSV
   *
   * @param H5PChallenge $challenge
   *
   * @return Response
   *   Challenge results file
   */
  public function resultsCsv(H5PChallenge $challenge): Response {
    $response = new Response($this->challengeService->getChallengeResultsCsv($challenge));

    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition','attachment; filename="results.csv"');

    return $response;
  }

  /**
   * List of currently present challenges
   *
   * @return array
   *   Page structure
   * @throws EntityMalformedException
   */
  public function reportsList(): array {
    /** @var PagerSelectExtender $query */
    $query = $this->database
      ->select('h5p_challenge', 'c', [
        'fetch' => H5PChallenge::class,
      ])
      ->extend(PagerSelectExtender::class);
    $query->fields('c', ['uuid', 'content_id', 'title', 'email', 'started', 'finished', 'code', 'results_sent', 'langcode', 'user_id',]);
    $query->orderBy('c.started', 'DESC');
    $result = $query->limit(25)->execute();

    $header = [
      $this->t('Title'),
      $this->t('Started'),
      $this->t('Finished'),
      $this->t('Code'),
      $this->t('Results sent'),
      $this->t('Language'),
      $this->t('Creator'),
    ];
    $rows = [];

    foreach ($result->fetchAll() as $challenge) {
      $user = $challenge->user_id ? User::load($challenge->user_id) : NULL;

      $rows[] = [
        'data' => [
          'title' => Link::createFromRoute(
            $challenge->title,
            'h5p_challenge.h5p_challenge_controller_results',
            ['challenge' => $challenge->uuid],
            [
              'attributes' => [
                'target' => '_blank',
              ]
            ]
          ),
          'started' => $this->dateFormatter->format($challenge->started, 'long'),
          'finished' => $this->dateFormatter->format($challenge->finished, 'long'),
          'code' => $challenge->code,
          'results_sent' => ($challenge->results_sent) ? $this->t('Yes') : $this->t('No'),
          'language' => $this->languageManager->getLanguageName($challenge->langcode),
          'creator' => $user ? $user->toLink() : '',
        ],
      ];
    }

    $complete_query = $this->database->select('h5p_challenge_statistics');

    $active_query = $this->database->select('h5p_challenge');
    $active_query->condition('finished', time(), '>=');

    $player_query = $this->database->select('h5p_challenge_statistics');
    $player_query->addExpression('SUM(player_count)', 'total');

    $response['statistics'] = [
      '#type' => 'table',
      '#caption' => $this->t('Statistics'),
      '#header' => [
        $this->t('Type'),
        $this->t('Count'),
      ],
      '#rows' => [
        [
          'data' => [
            'type' => $this->t('Complete challenges'),
            'count' => $complete_query->countQuery()->execute()->fetchField(),
          ],
        ],
        [
          'data' => [
            'type' => $this->t('Active challenges'),
            'count' => $active_query->countQuery()->execute()->fetchField(),
          ],
        ],
        [
          'data' => [
            'type' => $this->t('Player count'),
            'count' => $player_query->execute()->fetchField(),
          ],
        ],
      ],
    ];

    $response['challenges'] = [
      '#type' => 'table',
      '#caption' => $this->t('Challenges'),
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No challenges found'),
    ];

    $response['pager'] = [
      '#type' => 'pager',
    ];

    return $response;
  }

  /**
   * List of challenges created by user
   *
   * @return array
   *   Page structure
   * @throws Exception
   */
  public function mine(): array {
    /** @var PagerSelectExtender $query */
    $query = $this->database
      ->select('h5p_challenge', 'c', [
        'fetch' => H5PChallenge::class,
      ])
      ->extend(PagerSelectExtender::class);
    $query->condition('c.user_id', $this->currentUser->id(), '=');
    $query->fields('c', ['uuid', 'content_id', 'title', 'email', 'started', 'finished', 'code', 'results_sent', 'langcode',]);
    $query->orderBy('c.started', 'DESC');
    $result = $query->limit(25)->execute();

    $header = [
      $this->t('Title'),
      $this->t('Active'),
      $this->t('Started'),
      $this->t('Finished'),
      $this->t('Code'),
      $this->t('Results sent'),
      $this->t('Language'),
      $this->t('Participants'),
    ];
    $rows = [];
    $challenges = $result->fetchAll();

    if ($challenges) {
      $count_query = $this->database->select('h5p_challenge_points', 'cp');
      $count_query->fields('cp', ['challenge_uuid']);
      $count_query->addExpression('COUNT(*)', 'count');
      $count_query->groupBy('challenge_uuid');
      $count_query->condition('cp.challenge_uuid', array_map(function($challenge) {
        return $challenge->uuid;
      }, $challenges), 'IN');
      $participant_counts = $count_query->execute()->fetchAllKeyed(0, 1);

      foreach ($challenges as $challenge) {
        $rows[] = [
          'id' => 'challenge-' . $challenge->uuid,
          'data' => [
            'title' => Link::createFromRoute(
              $challenge->title,
              'h5p_challenge.h5p_challenge_controller_results',
              ['challenge' => $challenge->uuid],
              []
            ),
            'active' => $challenge->isActive() ? $this->t('Yes') : $this->t('No'),
            'started' => $this->dateFormatter->format($challenge->started, 'long'),
            'finished' => $this->dateFormatter->format($challenge->finished, 'long'),
            'code' => $challenge->code,
            'results_sent' => ($challenge->results_sent) ? $this->t('Yes') : $this->t('No'),
            'language' => $this->languageManager->getLanguageName($challenge->langcode),
            'participants' => $participant_counts[$challenge->uuid] ?? 0,
          ],
        ];
      }
    }

    $response['challenges'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No challenges found'),
    ];

    $response['pager'] = [
      '#type' => 'pager',
    ];

    return $response;
  }

  /**
   * Responds with challenge JS settings
   *
   * @return JsonResponse
   *   JSON response with challenge settings
   */
  public function settings(): JsonResponse {
    $settings = $this->challengeService->getJsSettings();

    return new JsonResponse($settings);
  }

}
