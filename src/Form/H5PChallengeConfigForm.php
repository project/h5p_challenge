<?php

namespace Drupal\h5p_challenge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class H5PChallengeConfigForm.
 */
class H5PChallengeConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'h5p_challenge.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'h5p_challenge_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('h5p_challenge.config');

    $form['recaptcha'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('reCaptcha Settings'),
      '#description' => 'Register visible reCaptcha V2.',
    ];
    $form['recaptcha']['recaptcha_site_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('recaptcha_site_key'),
      '#required' => TRUE,
    ];
    $form['recaptcha']['recaptcha_secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('recaptcha_secret_key'),
      '#required' => TRUE,
    ];

    $form['email_from_address'] = [
      '#type' => 'email',
      '#title' => $this->t('Email from address'),
      '#placeholder' => 'no-reply@some.domain',
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('email_from_address'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('h5p_challenge.config')
      ->set('recaptcha_site_key', $form_state->getValue('recaptcha_site_key'))
      ->set('recaptcha_secret_key', $form_state->getValue('recaptcha_secret_key'))
      ->set('email_from_address', $form_state->getValue('email_from_address'))
      ->save();
  }

}
