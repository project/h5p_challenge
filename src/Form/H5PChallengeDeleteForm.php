<?php

namespace Drupal\h5p_challenge\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultNeutral;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;

/**
 * Class H5PChallengeDeleteForm.
 */
class H5PChallengeDeleteForm extends FormBase {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var Connection
   */
  protected $database;

  /**
   * @var H5PChallenge
   */
  protected $challenge;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Checks if challenge can be deleted. Only challenges that have already finished and have results sent can be
   * deleted.
   *
   * @return boolean
   *   TRUE or FALSE
   */
  private function canDelete(): bool {
    return !$this->challenge->isActive() && (int)$this->challenge->results_sent === 1;
  }

  /**
   * Deletes challenge and responses
   * @throws \Exception
   */
  private function delete(): void {
    $this
      ->database
      ->delete('h5p_challenge_points')
      ->condition('challenge_uuid', $this->challenge->uuid, '=')
      ->execute();
    $this
      ->database
      ->delete('h5p_challenge')
      ->condition('uuid', $this->challenge->uuid, '=')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'h5p_challenge_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, H5PChallenge $challenge = NULL) {
    $this->challenge = $challenge;

    $form['body'] = [
      '#type' => 'container',
    ];
    $form['body']['warning'] = [
      '#prefix' => '<strong>',
      '#suffix' => '</strong>',
      '#plain_text' => $this->t('This will remove the challenge itself and all of its player responses. This can not be undone!'),
    ];

    if (!$this->canDelete()) {
      $form['body']['hr'] = [
        '#type' => 'markup',
        '#markup' => '<hr>',
      ];
      $form['body']['can_not_delete_explanation'] = [
        '#prefix' => '<strong>',
        '#suffix' => '</strong>',
        '#plain_text' => $this->t('Challenge can not be removed for as long as it is active and results have not been sent yet.'),
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('h5p_challenge.h5p_challenge_controller_results', [
        'challenge' => $challenge->uuid,
      ]),
      '#attributes' => [
        'class' => [
          'button',
        ],
      ],
    ];

    if (!$this->canDelete()) {
      $form['actions']['submit']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->canDelete()) {
      $this->delete();
      $this->messenger()->addMessage($this->t('Challenge removed.'));
      $form_state->setRedirect('h5p_challenge.h5p_challenge_controller_mine');
    }
  }

  /**
   * Checks and limits access to a form.
   *
   * @param AccountInterface $account
   * @param H5PChallenge $challenge
   * @return AccessResult|AccessResultAllowed|AccessResultNeutral
   */
  public function access(AccountInterface $account, H5PChallenge $challenge) {
    return AccessResult::allowedIf(
      $account->isAuthenticated()
      && $challenge->belongsToUser()
      && $account->id() === $challenge->user_id
    );
  }

}
