<?php

namespace Drupal\h5p_challenge\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultNeutral;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;

/**
 * Class H5PChallengeEndForm.
 */
class H5PChallengeEndForm extends FormBase {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var Connection
   */
  protected $database;

  /**
   * @var H5PChallenge
   */
  protected $challenge;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Checks if challenge can be ended prematurely. Only active challenges can be ended.
   *
   * @return boolean
   *   TRUE or FALSE
   */
  private function canEnd(): bool {
    return $this->challenge->isActive();
  }

  /**
   * Ends challenge
   */
  private function end(): void {
    $this->database->update('h5p_challenge')
      ->fields([
        'finished' => time(),
      ])
      ->condition('uuid', $this->challenge->uuid)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'h5p_challenge_end_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, H5PChallenge $challenge = NULL) {
    $this->challenge = $challenge;

    $form['body'] = [
      '#type' => 'container',
    ];
    $form['body']['warning'] = [
      '#prefix' => '<strong>',
      '#suffix' => '</strong>',
      '#plain_text' => $this->t('This will end the challenge and can not be undone!'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('End'),
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('h5p_challenge.h5p_challenge_controller_results', [
        'challenge' => $challenge->uuid,
      ]),
      '#attributes' => [
        'class' => [
          'button',
        ],
      ],
    ];

    if (!$this->canEnd()) {
      $form['actions']['submit']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->canEnd()) {
      $this->end();
      $this->messenger()->addMessage($this->t('Challenge ended.'));
      $form_state->setRedirect('h5p_challenge.h5p_challenge_controller_results', [
        'challenge' => $this->challenge->uuid,
      ]);
    }
  }

  /**
   * Checks and limits access to a form.
   *
   * @param AccountInterface $account
   * @param H5PChallenge $challenge
   * @return AccessResult|AccessResultAllowed|AccessResultNeutral
   */
  public function access(AccountInterface $account, H5PChallenge $challenge) {
    return AccessResult::allowedIf(
      $account->isAuthenticated()
      && $challenge->belongsToUser()
      && $account->id() === $challenge->user_id
    );
  }

}
