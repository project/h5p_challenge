<?php

namespace Drupal\h5p_challenge;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailFormatHelper;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\h5p_challenge\FetchClass\H5PChallengePoints;
use Exception;

/**
 * Class H5PChallengeService.
 */
class H5PChallengeService implements H5PChallengeServiceInterface {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var Connection
   */
  protected Connection $database;

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Date formatter
   *
   * @var DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Mail manager
   *
   * @var MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * Current user
   *
   * @var AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Language manager
   *
   * @var LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a new H5PChallengeService object.
   */
  public function __construct(Connection $database, ConfigFactoryInterface $configFactory, DateFormatterInterface $dateFormatter, MailManagerInterface $mailManager, AccountProxyInterface $currentUser, LanguageManagerInterface $languageManager) {
    $this->database = $database;
    $this->configFactory = $configFactory;
    $this->dateFormatter = $dateFormatter;
    $this->mailManager = $mailManager;
    $this->currentUser = $currentUser;
    $this->languageManager = $languageManager;
  }

  /**
   * Returns a value from h5p_challenge config as a string or null if the value is not set.
   *
   * @param string $name
   * @return string|null
   */
  private function getConfigValue(string $name): ?string {
    $config = $this->configFactory->get('h5p_challenge.config');
    $value = $config->get($name);

    return is_string($value) ? $value : null;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecaptchaSiteKey(): ?string {
    return $this->getConfigValue('recaptcha_site_key');
  }

  /**
   * {@inheritdoc}
   */
  public function getRecaptchaSecretKey(): ?string {
    return $this->getConfigValue('recaptcha_secret_key');
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailFromAddress(): ?string {
    return $this->getConfigValue('email_from_address');
  }

  /**
   * {@inheritdoc}
   */
  public function generateRandomNumber(int $length): string {
    // Source: https://stackoverflow.com/a/13169091/2704169
    $fn = function_exists('random_int') ? 'random_int' : 'mt_rand';
    $result = '';

    for($i = 0; $i < $length; $i++) {
      $result .= $fn(0, 9);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function isUniqueChallengeCode(string $code, int $contentId): bool {
    $query = $this->database->select('h5p_challenge', 'c');
    $query->condition('c.content_id', $contentId);
    $query->condition('c.code', $code);
    $query->addExpression('COUNT(*)');
    $query->countQuery();
    $count = $query->execute()->fetchField();

    return (int)$count === 0;
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function generateUniqueChallengeCode(int $contentId): string {
    $iteration = 1;
    $length = 6;

    $code = $this->generateRandomNumber($length);

    while(!$this->isUniqueChallengeCode($code, $contentId)) {
      $code = $this->generateRandomNumber($length);
      $iteration++;
      if ($iteration > 3) {
        $length++;
        $iteration = 0;
      }
    }

    return $code;
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function checkIfContentExists(int $contentId): bool {
    $content = $this->database->select('h5p_content', 'c')
    ->fields('c', ['id',])
    ->condition('c.id', $contentId)
    ->execute()
    ->fetchObject();

    return !!$content;
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function getChallengeByContentIdAndCode(int $contentId, string $code): ?H5PChallenge {
    $result = $this->database->select('h5p_challenge', 'c', [
      'fetch' => H5PChallenge::class,
    ])
    ->fields('c')
    ->condition('c.content_id', $contentId)
    ->condition('c.code', $code)
    ->execute()
    ->fetch();

    return $result instanceof H5PChallenge ? $result : null;
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function getChallengePointsByUuid(string $uuid): ?H5PChallengePoints {
    $result = $this->database->select('h5p_challenge_points', 'cp', [
      'fetch' => H5PChallengePoints::class,
    ])
    ->fields('cp')
    ->condition('cp.player_uuid', $uuid)
    ->execute()
    ->fetch();

    return $result instanceof H5PChallengePoints ? $result : null;
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function getChallengeByUuid(string $uuid): ?H5PChallenge {
    $result =  $this->database->select('h5p_challenge', 'c', [
      'fetch' => H5PChallenge::class,
    ])
    ->fields('c')
    ->condition('c.uuid', $uuid)
    ->execute()
    ->fetch();

    return $result instanceof H5PChallenge ? $result : null;
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function getChallengeResultsCsv(H5PChallenge $challenge): string {
    $header = ['#', t('Name'), t('Mastery level'), t('Score'), t('Total score'),];
    $questions = $challenge->getQuestions();

    if (count($questions) > 0) {
      foreach ($questions as $question) {
        $header[] = $question['title'];
      }
    }

    $header[] = t('Started');
    $header[] = t('Finished');
    $header[] = t('Time taken');
    $header[] = t('Attempts');

    $fp = fopen('php://temp', 'w');
    fputcsv($fp, $header);
    $result = $this->database->select('h5p_challenge_points', 'cp', [
      'fetch' => H5PChallengePoints::class,
    ])
    ->fields('cp')
    ->condition('cp.challenge_uuid', $challenge->uuid)
    ->orderBy('cp.started', 'ASC')
    ->execute();

    if ($result) {
      $no = 1;
      while($single = $result->fetch()) {
        /** @var H5PChallengePoints $single */
        $tmp = [
          'no' => $no,
          'name' => $single->name,
          'mastery_level' => $this->masteryLevelText((int)$single->points, (int)$single->max_points, (int)$single->tries_count),
          'score' => $single->points ?? 0,
          'total_score' => $single->max_points ?? 0,
        ];

        if (count($questions) > 0) {
          $answers = $single->getAnswers();

          foreach ($questions as $question) {
            $tmp[$question['id']] = isset($answers[$question['id']]) ? $answers[$question['id']]['score'] . '/' . $answers[$question['id']]['maxScore'] : '';
          }
        }

        $tmp['started'] = $this->dateFormatter->format($single->started, 'long');
        $tmp['finished'] = $single->finished ? $this->dateFormatter->format($single->finished, 'long') : '';
        $tmp['time_taken'] = $this->userFriendlyTimeTaken($single->finished ? ($single->finished - $single->started) : 0);
        $tmp['attempts'] = $single->tries_count;

        fputcsv($fp, $tmp);
        $no++;
      }
    }

    rewind($fp);
    $file_content = stream_get_contents($fp);
    fclose($fp);

    return $file_content;
  }

  /**
   * Returns count of players that took part in a challenge
   * @return int
   *   Count of players
   * @throws Exception
   */
  private function getChallengePlayerCount(H5PChallenge $challenge): int {
    $query = $this->database->select('h5p_challenge_points', 'cp');
    $query->condition('cp.challenge_uuid', $challenge->uuid);
    $query->addExpression('COUNT(*)');
    $query->countQuery();
    return (int)$query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function notificationsCron(): void {
    $result = $this->database->select('h5p_challenge', 'c', [
      'fetch' => H5PChallenge::class,
    ])
    ->fields('c')
    ->condition('c.results_sent', 0)
    ->condition('c.finished', time(), '<')
    ->execute();

    if ($result) {
      while($challenge = $result->fetch()) {
        /** @var H5PChallenge $challenge */
        $url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_results', [
          'challenge' => $challenge->uuid,
        ], [
          'absolute' => TRUE,
        ])->toString();
        $csv = Url::fromRoute('h5p_challenge.h5p_challenge_controller_results_csv', [
          'challenge' => $challenge->uuid,
        ], [
          'absolute' => TRUE,
        ])->toString();

        if ($challenge->belongsToUser()) {
          $message = t("One of your challenges has ended: @title.\nThe title of a resource for this challenge is: @ctitle.\n\nThe challege was active from @started to @finished.\n\nYou can access results at: @results\nOr download the CSV file at: @csv\n\nThe results page will never expire and you can always access it from the list of your challenges.\n\n\nBest regards,\nChallenge team.\n\n\n", [
            '@title' => $challenge->title,
            '@ctitle' => $challenge->getContentTitle(),
            '@started' => $this->dateFormatter->format($challenge->started, 'long'),
            '@finished' => $this->dateFormatter->format($challenge->finished, 'long'),
            '@results' => $url,
            '@csv' => $csv,
          ], [
            'langcode' => $challenge->langcode,
          ]);
        } else {
          $message = t("One of your challenges has ended: @title.\nThe title of a resource for this challenge is: @ctitle.\n\nThe challege was active from @started to @finished.\n\nYou can access results at: @results\nOr download the CSV file at: @csv\n\nThe results link would still be active for at least 36 hours after the challenge ends.\n\n\nBest regards,\nChallenge team.\n\n\n", [
            '@title' => $challenge->title,
            '@ctitle' => $challenge->getContentTitle(),
            '@started' => $this->dateFormatter->format($challenge->started, 'long'),
            '@finished' => $this->dateFormatter->format($challenge->finished, 'long'),
            '@results' => $url,
            '@csv' => $csv,
          ], [
            'langcode' => $challenge->langcode,
          ]);
        }

        $params = [
          'body' => (string) $message,
          'plaintext' => MailFormatHelper::wrapMail((string) $message),
          'attachment' => [
            'filecontent' => $this->getChallengeResultsCsv($challenge),
            'filename' => 'results.csv',
            'filemime' => 'text/csv',
          ],
        ];
        $this->mailManager->mail('h5p_challenge', 'challenge_ended', $challenge->email, $challenge->langcode, $params, NULL, TRUE);
        $this->database->update('h5p_challenge')
        ->fields([
          'results_sent' => 1,
        ])
        ->condition('uuid', $challenge->uuid)
        ->execute();

        $this->database->insert('h5p_challenge_statistics')
        ->fields([
          'content_id' => $challenge->content_id,
          'started' => $challenge->started,
          'finished' => $challenge->finished,
          'player_count' => $this->getChallengePlayerCount($challenge),
        ])
        ->execute();
      }
    }
  }

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  public function cleanupCron(): void {
    $finished = time() - (36 * 60 * 60);

    $ids = $this->database->select('h5p_challenge', 'c')
      ->fields('c', ['uuid',])
      ->condition('c.finished', $finished, '<')
      ->isNull('c.user_id')
      ->execute()
      ->fetchCol(0);

    if (is_array($ids) && count($ids) > 0) {
      $this->database->delete('h5p_challenge_points')
        ->condition('challenge_uuid', $ids, 'IN')
        ->execute();
      $this->database->delete('h5p_challenge')
        ->condition('uuid', $ids, 'IN')
        ->execute();
    }

    $this->database->update('h5p_challenge')
      ->condition('finished', $finished, '<')
      ->isNotNull('user_id')
      ->fields([
        'code' => NULL,
      ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalization(): array {
    return [
      'challenge' => t('Challenge'),
      'challengeDescription' => t('Create new or join an existing one'),
      'headerChallenge' => t('Challenge'),
      'buttonJoin' => t('Join (student)'),
      'buttonCreate' => t('Create (teacher)'),
      'buttonGenerateCode' => t('Generate code'),
      'buttonStart' => t('Start'),
      'buttonFinish' => t('Finish'),
      'labelTitle' => t('Title'),
      'labelTeacherEmail' => t('Teacher email'),
      'labelChallengeDuration' => t('Duration'),
      'labelName' => t('Name'),
      'labelCodeInput' => t('Enter code'),
      'placeholderName' => t('Name'),
      'placeholderTitle' => t('Title'),
      'placeholderEmail' => t('some.one@some.provider'),
      'optionOneHour' => t('One hour'),
      'optionTwoHours' => t('Two hours'),
      'optionOneDay' => t('One day'),
      'optionOneWeek' => t('One week'),
      'textChallengeExplanation' => t('NB! Challenge results will be automatically stored after you answer all the questions.'),
      'textChallengeTitle' => t('Challenge: @title'),
      'textChallengeStarted' => t('Challenge started: @date'),
      'textChallengeEnds' => t('Until challenge ends: @timer'),
      'confirmEndChallenge' => t('Clicking "Finish" button will end the Challenge. Score will be stored only if all the questions are answered.'),
      'confirmEndChallengeCanGetScore' => t('Clicking "Finish" button will end the Challenge. We will try to store the current score, provided that one is available.'),
      'errorCouldNotJoinTheChallenge' => t('Something went wrong! Challenge could not be joined.'),
      'errorCouldNotStartNewChallenge' => t('Something went wrong! Challenge could not be created.'),
      'errorCouldNotEndChallenge' => t('Something went wrong! Could not finish the Challenge.'),
      'errorUnknown' => t('Service error! Please try again or contact the administrator.'),
      'successScoreSubmitted' => t('You have successfully completed the Challenge. You could try again or click the "Finish" button.'),
      'challengeUrl' => t('Results URL: @url'),
      'textSupportedHint' => t('Current content is able to store results for Challenge participants.'),
      'textCheckHint' => t('Please make sure that current content is able to store results for Challenge participants. Try it out for yourself to be sure.'),
      'textAuthAndAnonHint' => t('Anonymous user can access Challenge results during a chosen period (1 hour to 1 week) with an additional period of 36 hours. This limitation does not affect Challenges created by authenticated users.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function userFriendlyTimeTaken(int $time_taken): string {
    if ($time_taken === 0) {
      return '';
    }

    $hours = 0;

    if ($time_taken > 3600) {
      $hours = floor($time_taken / 3600);
      $time_taken = $time_taken - $hours * 3600;
    }

    $minutes = ceil($time_taken / 60);

    if ($hours && $minutes) {
      return t(':hoursh :minutesm', [
        ':hours' => $hours,
        ':minutes' => $minutes,
      ]);
    }

    return t(':minutesm', [
      ':minutes' => $minutes,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function masteryLevelText(int $points, int $max_points, int $attempts): string {
    if ($max_points === 0 || $attempts === 0) {
      return t('Not finished yet.');
    }

    $score_percent = $points / $max_points * 100;

    if ($score_percent === 100 && $attempts === 1) {
      return t('Maximum score on first attempt.');
    } else if ($score_percent > 51 && $attempts <= 2 ) {
      return t('Score above 51% with two or less attempts.');
    } else {
      // $score_percent < 50 || $attempts > 2
      return t('Score below 50% or more than two attempts.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getJsSettings(): array {
    $settings = [
      'recaptchaSiteKey' => $this->getRecaptchaSiteKey(),
      'l10n' => $this->getLocalization(),
      'language' => $this->languageManager->getCurrentLanguage()->getId(),
    ];

    if ($this->currentUser->isAuthenticated()) {
      $settings['user']['display_name'] = $this->currentUser->getDisplayName();
      $settings['user']['mail'] = $this->currentUser->getEmail();
    }

    return $settings;
  }

}
