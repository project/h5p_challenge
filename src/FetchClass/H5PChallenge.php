<?php

namespace Drupal\h5p_challenge\FetchClass;

use ArrayIterator;
use Drupal\h5p\Entity\H5PContent;

/**
 * Class H5PChallenge
 */
class H5PChallenge {

  // These types were taken from H5P.Column codebase and are considered to be tasks
  const TYPES_WITH_SCORE = [
    'H5P.ImageHotspotQuestion',
    'H5P.Blanks',
    'H5P.Essay',
    'H5P.SingleChoiceSet',
    'H5P.MultiChoice',
    'H5P.TrueFalse',
    'H5P.DragQuestion',
    'H5P.Summary',
    'H5P.DragText',
    'H5P.MarkTheWords',
    'H5P.MemoryGame',
    'H5P.QuestionSet',
    'H5P.InteractiveVideo',
    'H5P.CoursePresentation',
    'H5P.DocumentationTool',
    'H5P.MultiMediaChoice',
  ];

  /**
   * @var string
   */
  public $uuid;

  /**
   * @var int
   */
  public $content_id;

  /**
   * @var string
   */
  public $title;

  /**
   * @var string
   */
  public $email;

  /**
   * @var int
   */
  public $started;

  /**
   * @var int
   */
  public $finished;

  /**
   * @var string|null
   */
  public $code;

  /**
   * @var int
   */
  public $results_sent;

  /**
   * @var string
   */
  public $langcode;

  /**
   * @var int|null
   */
  public $user_id;

  /**
   * @var string|null
   */
  public $data;

  /**
   * Returns content title by content identifier. Title of H5PContent entity is
   * used, not the title of the container Node or Entity.
   * @param  int    $id
   *   Content identifier
   *
   * @return string
   *   Content title or empty string
   */
  public static function getContentTitleById(int $id) : string {
    $content = H5PContent::load($id);

    if ($content) {
      return $content->get('title')->value;
    }

    return '';
  }

  /**
   * Determines sub content with possibility of having a score. Only top level
   * children are considered, not questions within questions. Returns an array
   * with all embedded content that is considered to be questions.
   *
   * @param H5PContent $entity
   *  H5PContent entity
   *
   * @return array
   *  An array with id = subContentId, type = library with version removed and
   *  title = metadata.title
   */
  public static function getContentQuestions(H5PContent $entity) : array {
    $response = [];
    $json = $entity->getFilteredParameters();
    $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator(json_decode($json), \RecursiveIteratorIterator::SELF_FIRST));

    foreach($iterator as $key => $value) {
      if ($key === 'subContentId' && $iterator->getDepth() <= 3) {
        /** @var ArrayIterator $inner_iterator */
        $inner_iterator = $iterator->getInnerIterator();
        $si_data = $inner_iterator->getArrayCopy();
        $si_library = explode(' ', trim($si_data['library']))[0];

        if (in_array($si_library, self::TYPES_WITH_SCORE)) {
          $response[$si_data['subContentId']] = [
            'id' => $si_data['subContentId'],
            'type' => $si_data['library'],
            'title' => $si_data['metadata']->title,
          ];
        }
      }
    }

    return $response;
  }

  /**
   * Determines if current challenge is still active.
   *
   * @return boolean
   *   Active or not
   */
  public function isActive() : bool {
    return (int)$this->finished > time();
  }

  /**
   * Returns content title used for current challenge.Title of H5PContent
   * entity is used, not the title of the container Node or Entity.
   *
   * @return string
   *   Content title or empty string
   */
  public function getContentTitle() : string {
    $entity = $this->getContentEntity();

    if ($entity) {
      return $entity->get('title')->value;
    }

    return $this->getData()['content']['title'] ?? '';
  }

  /**
   * Returns data or empty array if not set
   *
   * @return array
   *   Data or an empty array
   */
  private function getData() : array {
    if (isset($this->data) && $this->data) {
      return unserialize($this->data);
    }

    return [];
  }

  /**
   * Returns questions data set during initial creation. Please see
   * getContentQuestions() method for more details.
   *
   * @return array
   *   Questions data or an empty array
   */
  public function getQuestions() : array {
    $data = $this->getData();

    if (isset($data['content']['questions']) && is_array($data['content']['questions'])) {
      return $data['content']['questions'];
    }

    return [];
  }

  /**
   * Returns all identifiers from questions.
   *
   * @return array
   *   All question identifiers or an empty array
   */
  public function getQuestionIds() : array {
    return array_keys($this->getQuestions());
  }

  /**
   * Determines if challenge was belongs to a user or was created anonymously.
   *
   * @return boolean
   *   TRUE there is a user_id, FALSE otherwise
   */
  public function belongsToUser() : bool {
    return !!$this->user_id;
  }

  /**
   * Loads H5PContent entity
   *
   * @return \Drupal\h5p\Entity\H5PContent|null
   *   H5PContent entity if present, otherwise NULL
   */
  public function getContentEntity() : ?H5PContent {
    return H5PContent::load($this->content_id);
  }

}
