<?php

namespace Drupal\h5p_challenge\FetchClass;

/**
 * Class H5PChallengePoints
 */
class H5PChallengePoints {

  /**
   * @var string
   */
  public $challenge_uuid;

  /**
   * @var string
   */
  public $player_uuid;

  /**
   * @var string
   */
  public $name;

  /**
   * @var int
   */
  public $started;

  /**
   * @var int
   */
  public $finished;

  /**
   * @var int|null
   */
  public $points;

  /**
   * @var int|null
   */
  public $max_points;

  /**
   * @var int
   */
  public $tries_count;

  /**
   * @var int|null
   */
  public $user_id;

  /**
   * @var string|null
   */
  public $data;

  /**
   * Returns data or empty array if not set
   *
   * @return array
   *   Data or an empty array
   */
  private function getData() : array {
    if (isset($this->data) && $this->data) {
      return unserialize($this->data);
    }

    return [];
  }

  /**
   * Returns stored answers data from captured xAPI statements
   *
   * @return array
   *   Answers data or an empty array
   */
  public function getAnswers() : array {
    $data = $this->getData();

    if (isset($data['answers']) && is_array($data['answers'])) {
      return $data['answers'];
    }

    return [];
  }

  /**
   * Determines if there are answers for all required questions. Returns TRUE if
   * question identifiers array is empty.
   *
   * @param  array   $identifiers
   *   Question identifiers
   *
   * @return boolean
   *   TRUE if there are answers for all question identifiers, FALSE otherwise
   */
  public function hasAllQuestionsAnswered(array $identifiers) : bool {
    if (count($identifiers) === 0) {
      return TRUE;
    }

    $answers = $this->getAnswers();

    return count(array_diff($identifiers, array_keys($answers))) === 0;
  }

}
