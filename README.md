# H5P Challenge module

A H5P Challenge module for Drupal 8+. Provides a simple Challenge functionality to
be used with created H5P content. Challenge will automatically attach itself to
any H5P content view and add a new action button at the bottom. Challenges could
be created and taken part in by any user. Creation is protected by reCAPTCHA as
it will also send an email notification to the creator. Another notification
will be sent once the challenge has ended.

Challenge will internally use xAPI events and store results in a similar manner
to how H5P does that. Challenges should be functional in both internal and
external (iframe) contexts.

**NB!** Please note that Challenge will not be able to determine if the H5P
content is able to send the results or not. This means that Challenge will
be active for content that will not really have any results to send.

**NB!** Module code would currently assume that field with H5P content is
attached to a Node and would try to use the title of that node within
notifications being sent. This is a bad assumption as the Entity Reference field
could be added to any of the entities within the system.

## Installation

1. Install and activate the module
1. Visit `admin/config/system/h5p_challenge` and provide necessary configuration
1. Make sure that cron is being automatically triggered with hourly (preferably)
intervals so that the system would be able to send notifications for ended
challenges and perform the clean-up procedures
1. (Optional) Allow system to send file attachments
  * Install and activate
  [Mail System](https://www.drupal.org/project/mailsystem) and
  [Mime Mail](https://www.drupal.org/project/mimemail) modules
  * Configure `Mime Mail` to send messages in `Plain Text`
  * Create `H5P Challenge` specific configuration for `Mail System`, make sure
  that formatter is set to `Mime Mail Mailer` and the sender could be either
  default or anything used by the system (do not choose `Mime Mailer` as sender)
  * If you wan to target a specific email message with attachments, then also
  specify the key value of `challenge_ended`
  * Other configurations, modules or combinations of both could work as well,
  the mail focus in on Drupal mailing system to be able to send attachments

## Usage

1. Create H5P content that is able to send the score
1. Visit the content and start a challenge
1. Provide URL with instructions on how to locate your content from the system
and the challenge code itself to the students

## TODO

1. Add Estonian translation

## Running tests

Current code of H5P module has an issue with incomplete schema definition that is missing `h5p_site_type` from
definitions. This prevents challenge creation tests from passing.
```yaml
  h5p_site_type:
    type: string
    label: 'Site type'
```
