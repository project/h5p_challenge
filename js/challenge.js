(function () {
  "use strict";
  if (!window.H5P) {
    return;
  }

  /**
   * Returns baseUrl extracted from setFinished AJAX URL that will also contain the language override.
   * URL might also contain some other path prefix values.
   *
   * @returns {string} Returns the equivalent of baseUrl + pathPrefix from drupalSettings.path
   */
  function getBaseUrl() {
    return H5PIntegration.ajax.setFinished.split("h5p-ajax/")[0];
  }

  /**
  * @class
  * @augments H5P.EventDispatcher
  * @param {Object} instance H5P content instance
  */
  H5P.Challenge = (function ($, EventDispatcher) {
    /**
     * Challenge answers class
     * @constructor
     */
    function ChallengeAnswers() {
      this.answers = {};
    }

    /**
     * Returns answers data
     *
     * @return {object}
     */
    ChallengeAnswers.prototype.getAnswers = function() {
      return this.answers;
    };

    /**
     * Checks if answer exists for provided identifier
     *
     * @param  {string} id Question identifier
     *
     * @return {boolean}
     */
    ChallengeAnswers.prototype.hasAnswerFor = function(id) {
      return this.answers.hasOwnProperty(id);
    };

    /**
     * Adds an answer data, overwrited existing one while incrementing the
     * attempts count
     *
     * @param  {string} id       Question identifier
     * @param  {int}    score    Player score
     * @param  {int}    maxScore Maximum possible score
     * @param  {string} duration Duration reported by xAPI
     */
    ChallengeAnswers.prototype.addAnswer = function(id, score, maxScore, duration) {
      this.answers[id] = {
        id: id,
        score: score,
        maxScore: maxScore,
        duration: duration,
        attempts: this.hasAnswerFor(id) ? this.answers[id]['attempts'] + 1 : 1
      };
    };

    /**
     * Resets answers data store to an empty object
     */
    ChallengeAnswers.prototype.resetAnswers = function() {
      this.answers = {};
    };

    function Challenge(instance, settings) {
      EventDispatcher.call(this);

      /** @alias H5P.Challenge **/
      var self = this;

      var questionAnswers = new ChallengeAnswers();

      var countdownInterval;

      // DOM Elements
      var $container = H5P.jQuery('.h5p-content[data-content-id="' + instance.contentId + '"]').get(0);
      var $actions = H5P.jQuery($container).find('.h5p-actions');
      var $challenge = H5P.jQuery('<div class="challenge-container" style="display:none;"></div>');

      /**
       * Translation function, uses H5P.t function with preset domain.
       *
       * @param  {string} key  Translation key
       * @param  {object} vars Replacement variables
       * @return {string}
       */
      self.t = function(key, vars) {
        return H5P.t(key, vars, 'Challenge');
      };

      /**
       * Determines if H5P content allows getting score and maximum score values.
       *
       * @returns {boolean}
       */
      self.canGetScore = function() {
        return instance.getScore
          && typeof instance.getScore === 'function'
          && instance.getMaxScore
          && typeof instance.getMaxScore === 'function';
      };

      /**
       * Determines if H5P content allows scoring and maximum score is above zero.
       *
       * @returns {boolean}
       */
      self.hasUsableMaxScore = function() {
        return self.canGetScore()
          && instance.getMaxScore() > 0;
      };

      /**
      * Helper for creating action bar buttons.
      *
      * @private
      * @param {string} type
      * @param {string} customClass Instead of type class
      */
      var addActionBarButton = function (type, customClass) {
        /**
        * Handles selection of action
        */
        var handler = function () {
          self.trigger(type);
        };
        H5P.jQuery('<li/>', {
          'class': 'h5p-button h5p-noselect h5p-' + (customClass ? customClass : type),
          role: 'button',
          tabindex: 0,
          title: self.t(type + 'Description'),
          html: self.t(type),
          on: {
            click: handler,
            keypress: function (e) {
              if (e.which === 32) {
                handler();
                e.preventDefault(); // (since return false will block other inputs)
              }
            }
          },
          appendTo: $actions
        });
      };

      // Add to action bar and instanciate
      addActionBarButton('challenge');
      $challenge.insertBefore($actions);

      /**
       * Returns global challenge settings or an empy object
       *
       * @type {object}
       */
      self.getChallengeSettings = function() {
        return settings;
      };

      /**
       * Determines if user is authenticated
       *
       * @return {boolean}
       */
      self.isAuthenticatedUser = function() {
        return this.getChallengeSettings().hasOwnProperty('user');
      }

      /**
       * Returns a display name of an authenticated user or empty string
       *
       * @return {string}
       */
      self.getUserDisplayName = function() {
        return this.isAuthenticatedUser() ? this.getChallengeSettings().user.display_name : '';
      };

      /**
       * Returns an email address of an authenticated user or empty string
       *
       * @return {string}
       */
      self.getUserEmail = function() {
        return this.isAuthenticatedUser() ? this.getChallengeSettings().user.mail : '';
      };

      /**
       * Returns instance container element
       * @return {Node} DOM Node
       */
      self.getContainerDOMElement = function() {
        return $container;
      };

      /**
       * Returns challenge actions element
       * @return {Node} DOM Node
       */
      self.getActionsDOMElement = function () {
        return $challenge.find('.challenge-actions');
      };

      /**
       * Returns challenge container element
       * @return {Node} DOM Node
       */
      self.getDOMElement = function() {
        return $challenge;
      };

      /**
       * Returns challenge body element
       * @return {Node} DOM Node
       */
      self.getBodyDOMElement = function() {
        return $challenge.find('.challenge-body');
      };

      /**
       * Disables challenge buttons or ones within some context
       * @param  {Node} $context DOM Element to use a context
       */
      self.disableButtons = function($context) {
        if (!$context) {
          $context = $challenge;
        }

        $context.find('button').prop('disabled', true);
      };

      /**
       * Enables challenge buttons or ones within some context
       * @param  {Node} $context DOM Element to use as a context
       */
      self.enableButtons = function($context) {
        if (!$context) {
          $context = $challenge;
        }
        $context.find('button').prop('disabled', false);
      };

      /**
       * Remove validation class from challenge inputs or ones within some context
       * @param  {Node} $context DOM Element to use as a context
       */
      self.removeValidationClass = function($context) {
        if (!$context) {
          $context = $challenge;
        }
        $context.find('.missing-required-value').removeClass('missing-required-value');
      };

      /**
       * Returns input or select value by name with optional trim
       * @param  {string}  name Element name
       * @param  {boolean} trim Apply trim
       * @return {string}       Input value
       */
      self.getInputValue = function(name, trim, validate) {
        var $element = $challenge.find('input[name="' + name + '"], select[name="' + name + '"]');
        var value = $element.val();

        value = (trim === true) ? value.trim() : value;

        if (validate === true && !value) {
          $element.addClass('missing-required-value');
        }

        return value;
      };

      /**
       * Checks if there is an active challenge running
       * @return {boolean}
       */
      self.isActiveParticipation = function() {
        return !!self.getChallengeUUID();
      };

      /**
       * Returns running challenge UUID
       * @return {mixed} String or null
       */
      self.getChallengeUUID = function() {
        var cookieData = Cookies.getJSON('Drupal.visitor.h5p-challenge-for-' + instance.contentId);

        if (cookieData) {
          return cookieData.uuid;
        }

        return null;
      };

      /**
       * Returns challenge info
       * @return {mixed} Object or null
       */
      self.getChallengeInfo = function() {
        var cookieData = Cookies.getJSON('Drupal.visitor.h5p-challenge-for-' + instance.contentId);

        if (cookieData) {
          return cookieData.challenge;
        }

        return null;
      };

      /**
       * Checks if the challenge is finished (score stored)
       * @return {boolean} [description]
       */
      self.hasFinishedChallenge = function() {
        // TODO It is possible to use the getAnswerGiven method if the content type supports it
        // See this for more details: https://h5p.org/documentation/developers/contracts#guides-header-1
        var cookieData = Cookies.getJSON('Drupal.visitor.h5p-challenge-for-' + instance.contentId);

        if (cookieData) {
          return !!cookieData.finished;
        }

        return false;
      };

      /**
       * Returns security token query string, extracted from H5PIntegration.ajax.setFinished
       * @return {string} Query string with token data
       */
      self.getSecurityTokenQS = function() {
        return H5PIntegration.ajax.setFinished.substring(H5PIntegration.ajax.setFinished.indexOf('?token='));
      };

      /**
       * Extracts reCAPTCHA Site Key data from configuration provided by the main script
       * @return {string} Site Key or an empty string
       */
      self.getRecaptchaSiteKey = function() {
        return this.getChallengeSettings().recaptchaSiteKey;
      };

      /**
       * Submits challenge score
       * @param  {int}      score                Current score
       * @param  {int}      maxScore             Maximum score
       * @param  {string}   duration             Duration reported by xAPI
       * @param  {function} cb                   Callback function
       * @param  {boolean}  preventSuccessAlerts Prevent any success alert from being triggered
       */
      self.setFinished = function(score, maxScore, duration, cb, preventSuccessAlerts) {
        if (self.isActiveParticipation()) {
          preventSuccessAlerts = !!preventSuccessAlerts;

          H5P.jQuery.post(getBaseUrl() + 'h5p_challenge/set-finished.json' + self.getSecurityTokenQS(), {
            contentId: instance.contentId,
            uuid: self.getChallengeUUID(),
            score: score,
            maxScore: maxScore,
            duration: duration,
            questionAnswers: questionAnswers.getAnswers()
          }, function(response) {
            if (!response.success) {
              if (response.message) {
                alert(response.message);
              }
              return;
            } else if (!preventSuccessAlerts) {
              alert(self.t('successScoreSubmitted'));
            }
          }).fail(function(response) {
            alert(self.t('errorUnknown'));
          }).done(function() {
            if (cb && typeof cb === 'function') {
              cb();
            }
          });
        }
      };

      /**
       * Add visuals for active challenge
       */
      self.addActiveChallengeVisuals = function() {
        if (self.isActiveParticipation()) {
          var challengeInfo = self.getChallengeInfo();
          var $challengeActions = self.getActionsDOMElement();
          var $challengeBody = self.getBodyDOMElement();
          self.disableButtons($challengeActions);
          H5P.jQuery('<div/>', {
            class: 'explanation',
            text: self.t('textChallengeExplanation')
          }).appendTo($challengeBody);
          H5P.jQuery('<div/>', {
            html: self.t('textChallengeTitle', {'@title': '<strong>' + challengeInfo.title + '</strong>'}),
          }).appendTo($challengeBody);
          H5P.jQuery('<div/>', {
            text: self.t('textChallengeStarted', {'@date': new Date(challengeInfo.started * 1000).toLocaleString()}),
          }).appendTo($challengeBody);
          H5P.jQuery('<div/>', {
            html: self.t('textChallengeEnds', {'@timer': '<span class="timer"></span>'}),
          }).appendTo($challengeBody);
          H5P.jQuery('<button/>', {
            class: 'h5p-joubelui-button',
            type: 'button',
            text: self.t('buttonFinish'),
            on: {
              click: function() {
                var isFinished = self.hasFinishedChallenge();
                var canGetScore = self.canGetScore();
                var promptText = canGetScore ? 'confirmEndChallengeCanGetScore' : 'confirmEndChallenge';
                var confirmation = confirm(self.t(promptText));

                if (confirmation) {
                  if (canGetScore && !isFinished) {
                    self.disableButtons(self.getBodyDOMElement());
                    self.setFinished(instance.getScore(), instance.getMaxScore(), '', function() {
                      self.trigger('endChallenge');
                    }, true);
                  } else {
                    self.trigger('endChallenge');
                  }
                }
              }
            }
          }).appendTo($challengeBody);

          // Deal with timer
          var endTime = new Date(challengeInfo.finished * 1000).getTime();
          if ( countdownInterval) {
            clearInterval(countdownInterval);
          }
          countdownInterval = setInterval(function() {
            var now = new Date().getTime();

            var distance = endTime - now;

            if (distance < 0) {
              clearInterval(countdownInterval);
              $challengeBody.find('.timer').html(self.t('challengeExpired'));
            }

            var parts = {
              days: Math.floor(distance / (1000 * 60 * 60 * 24)),
              hours: Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
              minutes: Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
              seconds: Math.floor((distance % (1000 * 60)) / 1000)
            };

            var timerHtml = '';
            var showValue = false;
            H5P.jQuery.each(parts, function(key, value) {
              showValue = (showValue || value > 0) ? true : false;
              if (showValue) {
                timerHtml += (value < 10) ? '0' + value : value;
                timerHtml += (key !== 'seconds') ? ':' : '';
              }
            });

            $challengeBody.find('.timer').html(timerHtml);
          }, 1000);
        }
      };

      /**
       * Listens for 'challenge' event and deals with visuals
       */
      self.on('challenge', function() {
        if (countdownInterval) {
          clearInterval(countdownInterval);
        }

        if ($challenge.is(':visible')) {
          $challenge.hide();
          H5P.trigger(instance, 'resize');
          $challenge.html('');
          return;
        }

        $challenge.html('<div class="challenge-header"><label>' + self.t('headerChallenge') + '</label></div><div class="challenge-actions"></div><div class="challenge-body"></div>');

        H5P.jQuery('<button/>', {
          class: 'h5p-joubelui-button',
          type: 'button',
          text: self.t('buttonJoin'),
          on: {
            click: function() {
              self.trigger('joinExistingChallenge');
            }
          }
        }).appendTo(self.getActionsDOMElement());
        H5P.jQuery('<button/>', {
          class: 'h5p-joubelui-button',
          type: 'button',
          text: self.t('buttonCreate'),
          on: {
            click: function() {
              self.trigger('startNewChallenge');
            }
          }
        }).appendTo(self.getActionsDOMElement());

        if (self.isActiveParticipation()) {
          self.addActiveChallengeVisuals();
        }

        $challenge.show();
        H5P.trigger(instance, 'resize');
      });

      /**
       * Listens to 'joinExistingChallenge' event, creates one and deals with visuals
       */
      self.on('joinExistingChallenge', function() {
        if (self.isActiveParticipation()) {
          return;
        }

        var $tmpContainer = H5P.jQuery('<div/>', {
          class: 'join-existing-challenge',
        });
        H5P.jQuery('<div class="row"><label>' + self.t('labelName') + ':</label><input type="text" value="" placeholder="' + self.t('placeholderName') + '" name="participant-name"></div>').appendTo($tmpContainer);

        if (self.isAuthenticatedUser()) {
          $tmpContainer.find('input[name="participant-name"]').val(self.getUserDisplayName()).attr('disabled', true);
        }

        H5P.jQuery('<div class="row"><label>' + self.t('labelCodeInput') + ':</label><input type="text" value="" placeholder="872762" name="challenge-code"></div>').appendTo($tmpContainer);
        H5P.jQuery('<button/>', {
          class: 'h5p-joubelui-button',
          type: 'button',
          text: self.t('buttonStart'),
          on: {
            click: function() {
              self.disableButtons();
              self.removeValidationClass(self.getBodyDOMElement());
              var participantName = self.getInputValue('participant-name', true, true);
              var challengeCode = self.getInputValue('challenge-code', true, true).replace(/\s/g, ''); // The code could have spaces in it

              if (!(participantName && challengeCode)) {
                self.enableButtons();
                return;
              }

              H5P.jQuery.post(getBaseUrl() + 'h5p_challenge/start-playing.json' + self.getSecurityTokenQS(), {
                contentId: instance.contentId,
                name: participantName,
                code: challengeCode
              }, function(response) {
                self.enableButtons();

                if (!response.success) {
                  H5P.trigger(instance, 'resize');
                  if (response.message) {
                    alert(response.message);
                  } else {
                    alert(self.t('errorCouldNotJoinTheChallenge'));
                  }
                  return;
                }

                if (self.isActiveParticipation()) {
                  self.getBodyDOMElement().html('');
                  self.addActiveChallengeVisuals();
                  questionAnswers.resetAnswers();
                }

                H5P.trigger(instance, 'resize');
              }).fail(function() {
                self.enableButtons();
                H5P.trigger(instance, 'resize');
                alert(self.t('errorUnknown'));
              });
            }
          }
        }).appendTo($tmpContainer);
        self.getBodyDOMElement().html('');
        $tmpContainer.appendTo(self.getBodyDOMElement());
        H5P.trigger(instance, 'resize');
      });

      self.on('startNewChallenge', function() {
        if (self.isActiveParticipation()) {
          return;
        }

        var recaptchaResponse;
        var $tmpContainer = H5P.jQuery('<div/>', {
          class: 'start-new-challenge'
        });
        H5P.jQuery('<div>', {
          class: 'check-hint-text check-hint-text-bold',
          text: self.t(self.hasUsableMaxScore() ? 'textSupportedHint' : 'textCheckHint')
        }).appendTo($tmpContainer);
        H5P.jQuery('<div>', {
          class: 'check-hint-text',
          text: self.t('textAuthAndAnonHint')
        }).appendTo($tmpContainer);
        H5P.jQuery('<div class="row"><label>' + self.t('labelTitle') + ':</label><input type="text" value="" placeholder="' + self.t('placeholderTitle') + '" name="challenge-title"></div>').appendTo($tmpContainer);
        H5P.jQuery('<div class="row"><label>' + self.t('labelTeacherEmail') + ':</label><input type="email" value="" placeholder="' + self.t('placeholderEmail') + '" name="challenge-email"></div>').appendTo($tmpContainer);

        if (self.isAuthenticatedUser()) {
          $tmpContainer.find('input[name="challenge-email"]').val(self.getUserEmail()).prop('disabled', true);
        }

        H5P.jQuery('<div class="row"><label>' + self.t('labelChallengeDuration') + ':</label><select name="challenge-duration"><option value="1">' + self.t('optionOneHour') + '</option><option value="2">' + self.t('optionTwoHours') + '</option><option value="24">' + self.t('optionOneDay') + '</option><option value="168">' + self.t('optionOneWeek') + '</option></select></div>').appendTo($tmpContainer);
        H5P.jQuery('<div/>', {
          id: 'recaptcha-' + instance.contentId
        }).appendTo($tmpContainer);
        H5P.jQuery('<button/>', {
          class: 'h5p-joubelui-button',
          type: 'button',
          text: self.t('buttonGenerateCode'),
          on: {
            click: function() {
              self.disableButtons();
              self.removeValidationClass(self.getBodyDOMElement());
              var challengeTitle = self.getInputValue('challenge-title', true, true);
              var challengeEmail = self.getInputValue('challenge-email', true, true);
              var challengeDuration = self.getInputValue('challenge-duration', true, true);

              if (!(challengeTitle && challengeEmail && challengeDuration && recaptchaResponse)) {
                self.enableButtons();
                return;
              }
              H5P.jQuery.post(getBaseUrl() + 'h5p_challenge/create-new.json' + self.getSecurityTokenQS(), {
                contentId: instance.contentId,
                title: challengeTitle,
                email: challengeEmail,
                duration: challengeDuration,
                'g-recaptcha-response': recaptchaResponse
              }, function(response) {
                self.enableButtons();

                $challenge.find('.challenge-code').remove();
                $challenge.find('.challenge-results-url').remove();

                if (!response.success) {
                  H5P.trigger(instance, 'resize');
                  if (response.message) {
                    alert(response.message);
                  } else {
                    alert(self.t('errorCouldNotStartNewChallenge'));
                  }
                  return;
                }

                H5P.jQuery('<span/>', {
                  class: 'challenge-code',
                  text: response.code.substring(0, response.code.length / 2) + ' ' + response.code.substring(response.code.length / 2)
                }).appendTo($tmpContainer);
                H5P.jQuery('<span/>', {
                  class: 'challenge-results-url',
                  html: self.t('challengeUrl', {'@url': '<a href="' + response.url + '" target="_blank">' + response.url + '</a>'})
                }).appendTo($tmpContainer);
                H5P.trigger(instance, 'resize');
              }).fail(function() {
                self.enableButtons();
                H5P.trigger(instance, 'resize');
                alert(self.t('errorUnknown'));
              });
            }
          }
        }).appendTo($tmpContainer);
        self.getBodyDOMElement().html('');
        $tmpContainer.appendTo(self.getBodyDOMElement());
        H5P.trigger(instance, 'resize')

        grecaptcha.render('recaptcha-' + instance.contentId, {
          sitekey : self.getRecaptchaSiteKey(),
          callback: function(response) {
            recaptchaResponse = response;
          }
        });
        setTimeout(function() {
          H5P.trigger(instance, 'resize');
        }, 2000);
      });

      self.on('endChallenge', function(e) {
        if (!self.isActiveParticipation()) {
          // TODO See if we should destroy the cookie when one still exists
          return;
        }

        Cookies.remove('Drupal.visitor.h5p-challenge-for-' + instance.contentId);
        if (!self.isActiveParticipation()) {
          self.enableButtons(self.getActionsDOMElement());
          self.getBodyDOMElement().html('');
          H5P.trigger(instance, 'resize');
        }
        if (countdownInterval) {
          clearInterval(countdownInterval);
        }
      });

      // TODO Remove this approach as it seems to be old and abandoned
      H5P.on(instance, 'finish', function (event) {
        if (event.data !== undefined) {
          // This check is taken from the H5P core implementation, just in case that matters
          var validScore = typeof score === 'number' || score instanceof Number;
          if (validScore) {
            self.setFinished(event.data.score, event.data.maxScore, '');
          }
        }
      });

      H5P.on(instance, 'xAPI', function(event) {
        if ((event.getVerb() === 'completed' || event.getVerb() === 'answered')
        && !event.getVerifiedStatementValue(['context', 'contextActivities', 'parent'])
        && self.isActiveParticipation()) {
          var score = event.getScore();
          var maxScore = event.getMaxScore();
          var contentId = event.getVerifiedStatementValue(['object', 'definition', 'extensions', 'http://h5p.org/x-api/h5p-local-content-id']);

          // XXX This might be unneeded as the xAPI events are checked upon instance itself
          if (instance.contentId != contentId) {
            return;
          }

          self.setFinished(score, maxScore, event.getVerifiedStatementValue(['result', 'duration']));
        }
      });

      H5P.externalDispatcher.on('xAPI', function (event) {
        if ((event.getVerb() === 'completed' || event.getVerb() === 'answered')
        && event.getVerifiedStatementValue(['context', 'contextActivities', 'parent'])
        && event.getVerifiedStatementValue(['object', 'definition', 'extensions', 'http://h5p.org/x-api/h5p-local-content-id']) == instance.contentId
        && self.isActiveParticipation()) {
          questionAnswers.addAnswer(event.getVerifiedStatementValue(['object', 'definition', 'extensions', 'http://h5p.org/x-api/h5p-subContentId']), event.getScore(), event.getMaxScore(), event.getVerifiedStatementValue(['result', 'duration']));
        }
      });

      // Activate visuals in case of active participation
      if (self.isActiveParticipation()) {
        self.trigger('challenge');
      }
    }

    Challenge.prototype = Object.create(EventDispatcher.prototype);
    Challenge.prototype.constructor = Challenge;

    return Challenge;

  })(H5P.jQuery, H5P.EventDispatcher);

  H5P.jQuery('document').ready(function() {
    function init_h5p_challenge(settings) {
      H5PIntegration.l10n.Challenge = settings.l10n;

      if (H5P.instances && H5P.instances.length > 0) {
        // TODO Consider handling failures
        H5P.jQuery.when(
          H5P.jQuery.getScript('https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js'),
          H5P.jQuery.getScript('https://www.google.com/recaptcha/api.js?render=explicit&hl=' + settings.language)
        ).done(function() {
          H5P.jQuery.each(H5P.instances, function(index, instance) {
            var challenge = new H5P.Challenge(instance, settings);
          });
        });
      }
    }

    var settings;

    if (H5PIntegration) {
      if (window.drupalSettings && window.drupalSettings.h5p_challenge) {
        settings = window.drupalSettings.h5p_challenge;
      } else {
        try {
          if (window.top.drupalSettings && window.top.drupalSettings.h5p_challenge) {
            settings = window.top.drupalSettings.h5p_challenge;
          }
        } catch(err) {
          // Do nothing
        }
      }
    }

    if (settings) {
      init_h5p_challenge(settings);
    } else if (H5PIntegration) {
      H5P.jQuery.get(getBaseUrl() + 'h5p_challenge/settings.json', function(response) {
        init_h5p_challenge(response);
      }).fail(function() {
        console.error('Unable to load challenge settings!');
      });
    }
  });
})();
