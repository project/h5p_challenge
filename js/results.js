(function (Drupal, once) {
  Drupal.behaviors.h5pChallengeResults = {
    attach: function () {
      once('h5pChallengeResults', 'html').forEach(function(element) {
        const elements = element.querySelectorAll('[data-tippy-content]');

        if (elements.length > 0) {
          tippy(elements, {
            animation: 'scale',
            inertia: true
          });
        }
      });
    }
  };
})(Drupal, once);
