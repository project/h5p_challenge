<?php

declare(strict_types=1);

namespace Drupal\Tests\h5p_challenge\Functional;

use Behat\Mink\Exception\ElementNotFoundException;
use Behat\Mink\Exception\ExpectationException;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\Core\Url;
use Drupal\h5p\Entity\H5PContent;
use Drupal\h5p_challenge\H5PChallengeServiceInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;
use Exception;
use GuzzleHttp\Cookie\CookieJarInterface;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Exception\GuzzleException;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Http\Message\ResponseInterface;

/**
 * Test description.
 *
 * @group h5p_challenge
 */
final class H5PChallengeControllerTest extends BrowserTestBase {

  use ProphecyTrait;

  use AssertMailTrait {
    getMails as drupalGetMails;
  }

  const CHALLENGE_UUID = '946fc6a7-1e07-4fb1-a4b9-d96f28bc5578';
  const INACTIVE_CHALLENGE_UUID = '7bc89808-35d2-476f-b7e7-7ef00983be2b';
  const PLAYER_UUID = 'fb108cce-b29c-4958-8605-40f74fffc301';
  const MISSING_UUID = 'b717e213-6d32-4211-a444-8823f58267c0';
  const WRONG_USAGE_RESPONSE = [
    'success' => FALSE,
    'message' => 'Wrong usage',
  ];
  const NO_CHALLENGE_FOUND_RESPONSE = [
    'success' => FALSE,
    'message' => 'No challenge could be found for provided code',
  ];
  const NO_LONGER_ACTIVE_RESPONSE = [
    'success' => FALSE,
    'message' => 'Challenge is no longer active',
  ];
  const FAILURE_RESPONSE = [
    'success' => FALSE,
  ];
  const INVALID_DURATION_RESPONSE = [
    'success' => FALSE,
    'message' => 'Provided duration is invalid',
  ];
  const NO_CONTENT_RESPONSE = [
    'success' => FALSE,
    'message' => 'Content does not exist',
  ];
  const CAPTCHA_ERROR_RESPONSE = [
    'success' => FALSE,
    'message' => 'Captcha service responded with an error',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'h5p_challenge',
    'h5p_challenge_test',
  ];

  protected int $timestamp;
  protected ?Connection $connection;
  protected ?H5PChallengeServiceInterface $challengeService;
  protected ?UuidInterface $uuid;
  protected UserInterface $authenticatedUser;
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   * @throws EntityStorageException
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->timestamp = time();
    $this->authenticatedUser = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser(['access administration pages']);
    $this->connection = $this->container->get('database');
    $this->challengeService = $this->container->get('h5p.challenge.default');
    $this->uuid = $this->container->get('uuid');

    // Insert fake H5P library that is required for getting content questions data
    $this->connection->insert('h5p_libraries')
      ->fields([
        'machine_name' => 'H5P.TestLibrary',
        'title' => 'Test Library',
        'major_version' => 1,
        'minor_version' => 1,
        'patch_version' => 1,
        'runnable' => 1,
        'preloaded_js' => '',
        'preloaded_css' => '',
        'drop_library_css' => '',
        'semantics' => Json::encode([]),
        'tutorial_url' => '',
        'add_to' => '',
        'metadata_settings' => '',
      ])
      ->execute();

    // Create H5P content
    H5PContent::create([
      'library_id' => 1,
      'parameters' => Json::encode(['not' => 'empty']),
      'filtered_parameters' => Json::encode(['not' => 'empty']),
      'title' => 'Test content',
    ])->save();

    $challenges = [
      [
        'uuid' => self::CHALLENGE_UUID,
        'content_id' => 1,
        'title' => 'Test challenge',
        'email' => 'test@example.com',
        'started' => $this->timestamp,
        'finished' => $this->timestamp + 3600,
        'code' => '123456',
        'results_sent' => 0,
        'langcode' => 'en',
        'user_id' => $this->authenticatedUser->id(),
        'data' => NULL,
      ],
      [
        'uuid' => self::INACTIVE_CHALLENGE_UUID,
        'content_id' => 1,
        'title' => 'Inactive challenge',
        'email' => 'test@example.com',
        'started' => $this->timestamp - 7200,
        'finished' => $this->timestamp - 3600,
        'code' => '11223344',
        'results_sent' => 1,
        'langcode' => 'en',
        'user_id' => $this->authenticatedUser->id(),
        'data' => NULL,
      ],
    ];

    // Insert active and inactive challenges
    $challenge_query = $this->connection->insert('h5p_challenge')
      ->fields([
        'uuid',
        'content_id',
        'title',
        'email',
        'started',
        'finished',
        'code',
        'results_sent',
        'langcode',
        'user_id',
        'data',
      ]);

    foreach ($challenges as $challenge) {
      $challenge_query->values($challenge);
    }

    $challenge_query->execute();

    // Insert point for active challenge
    $this->connection->insert('h5p_challenge_points')
      ->fields([
        'challenge_uuid' => self::CHALLENGE_UUID,
        'player_uuid' => self::PLAYER_UUID,
        'name' => 'Test player',
        'started' => $this->timestamp + 600,
        'finished' => $this->timestamp + 1200,
        'points' => 0,
        'max_points' => 0,
        'tries_count' => 0,
        'user_id' => NULL,
        'data' => NULL,
      ])
      ->execute();
  }

  /**
   * Disable H5P export setting.
   *
   * @return void
   */
  protected function disableContentExport(): void {
    $config = $this->config('h5p.settings');
    $config
      ->set('h5p_export', 0)
      ->save();
  }

  /**
   * Return decoded JSON response.
   *
   * @param ResponseInterface $response
   * @return array
   */
  protected function getJsonResponseData(ResponseInterface $response): array {
    return Json::decode((string) $response->getBody());

  }

  /**
   * Check if challenge points database entry exists.
   *
   * @param string $challenge_uuid
   * @param string $player_uuid
   * @param string $name
   * @param UserInterface|null $user
   * @return bool
   */
  protected function challengePointsEntryExists(string $challenge_uuid, string $player_uuid, string $name, ?UserInterface $user): bool {
    $query = $this->connection->select('h5p_challenge_points', 'cp')
      ->fields('cp')
      ->condition('cp.challenge_uuid', $challenge_uuid)
      ->condition('cp.player_uuid', $player_uuid)
      ->condition('cp.name', $name);

    $user ? $query->condition('cp.user_id', $user->id()) : $query->isNull('cp.user_id');

      return 1 === (int) $query->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * Create POST request to challenge creation endpoint and return a response.
   *
   * @param array $form_params
   * @return ResponseInterface
   * @throws GuzzleException
   */
  protected function createChallenge(array $form_params): ResponseInterface {
    $http_client = $this->getHttpClient();
    $url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_create_new', [], [
      'query' => [
        'token' => 'token is ignored',
      ],
    ])
      ->setAbsolute()
      ->toString();

    return $http_client->request('POST', $url, [
      'form_params' => $form_params,
      'cookies' => $this->getSessionCookies(),
      'http_errors' => FALSE,
    ]);
  }

  /**
   * Create POST request to challenge start playing endpoint and return a response. Optionally use provided cookie jar
   * instance.
   *
   * @param array $form_params
   * @param CookieJarInterface|NULL $cookie_jar
   * @return ResponseInterface
   * @throws GuzzleException
   */
  protected function startPlaying(array $form_params, CookieJarInterface $cookie_jar = NULL): ResponseInterface {
    $http_client = $this->getHttpClient();
    $url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_start_playing', [], [
      'query' => [
        'token' => 'token is ignored',
      ],
    ])
      ->setAbsolute()
      ->toString();

    return $http_client->request('POST', $url, [
      'form_params' => $form_params,
      'cookies' => $cookie_jar ?? $this->getSessionCookies(),
      'http_errors' => FALSE,
    ]);
  }

  /**
   * Create POST request to challenge set finished endpoint and return a response. Optionally use provided cookie jar
   * instance.
   *
   * @param array $form_params
   * @param CookieJarInterface|NULL $cookie_jar
   * @return ResponseInterface
   * @throws GuzzleException
   */
  protected function setFinished(array $form_params, CookieJarInterface $cookie_jar = NULL): ResponseInterface {
    $http_client = $this->getHttpClient();
    $url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_set_finished', [], [
      'query' => [
        'token' => 'token is ignored',
      ],
    ])
      ->setAbsolute()
      ->toString();

    return $http_client->request('POST', $url, [
      'form_params' => $form_params,
      'cookies' => $cookie_jar ?? $this->getSessionCookies(),
      'http_errors' => FALSE,
    ]);
  }

  /**
   * Provide incorrect data for create new action test.
   * First argument would be the posted data and the second one expected response.
   *
   * @return array
   */
  public static function provideIncorrectCreateNewActionData(): array {
    return [
      'empty' => [
        [],
        self::WRONG_USAGE_RESPONSE,
      ],
      'only_content_id' => [
        [
          'contentId' => 1,
        ],
        self::WRONG_USAGE_RESPONSE,
      ],
      'content_id_and_title' => [
        [
          'contentId' => 1,
          'title' => 'New challenge',
        ],
        self::WRONG_USAGE_RESPONSE,
      ],
      'content_id_title_and_email' => [
        [
          'contentId' => 1,
          'title' => 'New challenge',
          'email' => 'test@localhost',
        ],
        self::WRONG_USAGE_RESPONSE,
      ],
      'still_no_captcha' => [
        [
          'contentId' => 1,
          'title' => 'New challenge',
          'email' => 'test@localhost',
          'duration' => 255,
        ],
        self::WRONG_USAGE_RESPONSE,
      ],
      'duration_under_limit' => [
        [
          'contentId' => 1,
          'title' => 'New challenge',
          'email' => 'test@example.com',
          'duration' => 0,
          'g-recaptcha-response' => 'recaptcha response',
        ],
        self::INVALID_DURATION_RESPONSE,
      ],
      'duration_over_limit' => [
        [
          'contentId' => 1,
          'title' => 'New challenge',
          'email' => 'test@example.com',
          'duration' => 169,
          'g-recaptcha-response' => 'recaptcha response',
        ],
        self::INVALID_DURATION_RESPONSE,
      ],
      'no_content' => [
        [
          'contentId' => 2,
          'title' => 'New challenge',
          'email' => 'test@example.com',
          'duration' => 1,
          'g-recaptcha-response' => 'recaptcha response',
        ],
        self::NO_CONTENT_RESPONSE,
      ],
      'captcha_error' => [
        [
          'contentId' => 1,
          'title' => 'New challenge',
          'email' => 'test@example.com',
          'duration' => 1,
          'g-recaptcha-response' => '500',
        ],
        self::CAPTCHA_ERROR_RESPONSE,
      ],
      'captcha_incorrect' => [
        [
          'contentId' => 1,
          'title' => 'New challenge',
          'email' => 'test@example.com',
          'duration' => 1,
          'g-recaptcha-response' => '204',
        ],
        self::CAPTCHA_ERROR_RESPONSE,
      ],
    ];
  }

  /**
   * Test create new challenge action with incorrect data and expecting failures.
   *
   * @dataProvider provideIncorrectCreateNewActionData
   * @param array $data
   * @param array $expectation
   * @return void
   * @throws GuzzleException
   */
  public function testCreateNewActionFailures(array $data, array $expectation): void {
    $response = $this->createChallenge($data);
    $this->assertSame($expectation, $this->getJsonResponseData($response));
  }

  /**
   * Provide correct data for create new action test.
   *
   * @return array[]
   */
  public static function provideCorrectCreateNewActionData(): array {
    return [
      'correct_data' => [
        [
          'contentId' => 1,
          'title' => 'New challenge',
          'email' => 'test@example.com',
          'duration' => 1,
          'g-recaptcha-response' => '200',
        ]
      ],
    ];
  }

  /**
   * Test create new challenge action with anonymous user and correct data and expecting success.
   *
   * @dataProvider provideCorrectCreateNewActionData
   * @param array $data
   * @return void
   * @throws GuzzleException
   */
  public function testSuccessCreateNewActionWithAnonymousUser(array $data): void {
    $this->disableContentExport();
    $response = $this->createChallenge($data);
    $this->assertEquals(200, $response->getStatusCode());
    $response_data = $this->getJsonResponseData($response);
    $this->assertTrue($response_data['success']);
    $this->assertNotEmpty($response_data['code']);
    $this->assertNotEmpty($response_data['url']);
    $challenge = $this->challengeService->getChallengeByContentIdAndCode(1, $response_data['code']);
    $this->assertNotNull($challenge);
    $this->assertEquals($data['title'], $challenge->title);
    $this->assertEquals($data['email'], $challenge->email);
    $this->assertEquals(3600, (int) $challenge->finished - (int) $challenge->started);

    $emails = $this->drupalGetMails([
      'module' => 'h5p_challenge',
      'key' => 'new_challenge_created',
    ]);
    $this->assertEquals(1, count($emails));
    $this->assertSame($data['email'], $emails[0]['to']);
    $this->assertSame('en', $emails[0]['langcode']);
  }

  /**
   * Test create new challenge action with authenticated user and correct data and expecting success.
   *
   * @dataProvider provideCorrectCreateNewActionData
   * @param array $data
   * @return void
   * @throws GuzzleException
   */
  public function testSuccessCreateNewActionWithAuthenticatedUser(array $data): void {
    $this->disableContentExport();
    $this->drupalLogin($this->authenticatedUser);
    $response = $this->createChallenge($data);
    $this->assertEquals(200, $response->getStatusCode());
    $response_data = $this->getJsonResponseData($response);
    $this->assertTrue($response_data['success']);
    $this->assertNotEmpty($response_data['code']);
    $this->assertNotEmpty($response_data['url']);
    $challenge = $this->challengeService->getChallengeByContentIdAndCode(1, $response_data['code']);
    $this->assertNotNull($challenge);
    $this->assertEquals($data['title'], $challenge->title);
    $this->assertEquals($this->authenticatedUser->getEmail(), $challenge->email);
    $this->assertEquals(3600, (int) $challenge->finished - (int) $challenge->started);

    $emails = $this->drupalGetMails([
      'module' => 'h5p_challenge',
      'key' => 'new_challenge_created',
    ]);
    $this->assertEquals(1, count($emails));
    $this->assertSame($data['email'], $emails[0]['to']);
    $this->assertSame('en', $emails[0]['langcode']);
  }

  /**
   * Provide incorrect data for start playing action test.
   * First argument would be the posted data and the second one expected response.
   *
   * @return array
   */
  public static function provideIncorrectStartPlayingActionData(): array {
    return [
      'empty' => [
        [],
        self::WRONG_USAGE_RESPONSE,
      ],
      'only_content_id' => [
        [
          'contentId' => 1,
        ],
        self::WRONG_USAGE_RESPONSE,
      ],
      'content_id_and_name' => [
        [
          'contentId' => 1,
          'name' => 'Test name',
        ],
        self::WRONG_USAGE_RESPONSE
      ],
      'incorrect_content_id' => [
        [
          'contentId' => 999,
          'name' => 'Test name',
          'code' => '123456',
        ],
        self::NO_CHALLENGE_FOUND_RESPONSE,
      ],
      'incorrect_code' => [
        [
          'contentId' => 1,
          'name' => 'Test name',
          'code' => '654321',
        ],
        self::NO_CHALLENGE_FOUND_RESPONSE,
      ],
      'inactive_challenge' => [
        [
          'contentId' => 1,
          'name' => 'Test name',
          'code' => '11223344',
        ],
        self::NO_LONGER_ACTIVE_RESPONSE,
      ],
    ];
  }

  /**
   * Test start playing action with incorrect data and expecting failures.
   *
   * @dataProvider provideIncorrectStartPlayingActionData
   * @param array $data
   * @param array $expectation
   * @return void
   * @throws GuzzleException
   */
  public function testStartPlayingActionFailures(array $data, array $expectation): void {
    $response = $this->startPlaying($data);
    $this->assertSame($expectation, $this->getJsonResponseData($response));
  }

  /**
   * Provide correct data for start playing action test.
   *
   * @return array[]
   */
  public static function provideCorrectStartPlayingActionData(): array {
    return [
      'correct_data' => [
        [
          'contentId' => 1,
          'name' => 'Test name',
          'code' => '123456',
        ]
      ],
    ];
  }

  /**
   * Test start playing action with anonymous user correct data and expecting success.
   *
   * @dataProvider provideCorrectStartPlayingActionData
   * @param array $data
   * @return void
   * @throws GuzzleException
   */
  public function testSuccessStartPlayingWithAnonymousUser(array $data): void {
    $cookies = $this->getSessionCookies();
    $response = $this->startPlaying($data, $cookies);
    $response_data = $this->getJsonResponseData($response);
    $this->assertTrue($response_data['success']);
    $this->assertNotEmpty($response_data['uuid']);
    $this->assertSame([
      'title' => 'Test challenge',
      'started' => strval($this->timestamp),
      'finished' => strval($this->timestamp + 3600),
    ], $response_data['challenge']);

    $cookie = $cookies->getCookieByName('Drupal.visitor.h5p-challenge-for-1');
    $this->assertNotNull($cookie, 'Challenge cookie not found');
    unset($response_data['success']);
    $this->assertSame(json_encode($response_data), urldecode($cookie->getValue()));
    $this->assertTrue($this->challengePointsEntryExists(self::CHALLENGE_UUID, $response_data['uuid'], $data['name'], NULL));
  }

  /**
   * Test start playing action with authenticated user and correct data and expecting success.
   *
   * @dataProvider provideCorrectStartPlayingActionData
   * @param array $data
   * @return void
   * @throws GuzzleException
   */
  public function testSuccessStartPlayingActionWithAuthenticatedUser(array $data): void {
    $this->drupalLogin($this->authenticatedUser);
    $cookies = $this->getSessionCookies();
    $response = $this->startPlaying($data, $cookies);
    $response_data = $this->getJsonResponseData($response);
    $this->assertTrue($response_data['success']);
    $this->assertNotEmpty($response_data['uuid']);
    $this->assertSame([
      'title' => 'Test challenge',
      'started' => strval($this->timestamp),
      'finished' => strval($this->timestamp + 3600),
    ], $response_data['challenge']);

    $cookie = $cookies->getCookieByName('Drupal.visitor.h5p-challenge-for-1');
    $this->assertNotNull($cookie, 'Challenge cookie not found');
    unset($response_data['success']);
    $this->assertSame(json_encode($response_data), urldecode($cookie->getValue()));
    $this->assertTrue($this->challengePointsEntryExists(self::CHALLENGE_UUID, $response_data['uuid'], $this->authenticatedUser->getDisplayName(), $this->authenticatedUser));
  }

  /**
   * Provide incorrect data for set finished action test.
   * First argument would be the posted data, the second one expected response and optional first the data of the
   * challenge points to be inserted into the database.
   *
   * @return array
   */
  public static function provideIncorrectSetFinishedActionData(): array {
    $timestamp = time();

    return [
      'empty' => [
        [],
        self::WRONG_USAGE_RESPONSE,
      ],
      'only_content_id' => [
        [
          'contentId' => 1,
        ],
        self::WRONG_USAGE_RESPONSE,
      ],
      'content_id_and_uuid' => [
        [
          'contentId' => 1,
          'uuid' => self::MISSING_UUID,
        ],
        self::WRONG_USAGE_RESPONSE,
      ],
      'score_as_null' => [
        [
          'contentId' => 1,
          'uuid' => self::MISSING_UUID,
          'score' => NULL,
        ],
        self::WRONG_USAGE_RESPONSE,
      ],
      'max_score_as_null' => [
        [
          'contentId' => 1,
          'uuid' => self::MISSING_UUID,
          'score' => 2,
          'maxScore' => NULL,
        ],
        self::WRONG_USAGE_RESPONSE,
      ],
      'missing_points_entry' => [
        [
          'contentId' => 1,
          'uuid' => self::MISSING_UUID,
          'score' => 2,
          'maxScore' => 4,
          'duration' => 'PT30S',
          'questionAnswers' => [],
        ],
        self::FAILURE_RESPONSE,
      ],
      'points_entry_with_missing_challenge' => [
        [
          'contentId' => 1,
          'uuid' => '0b40135f-2458-46c9-a0ff-98ed04db7dac',
          'score' => 2,
          'maxScore' => 4,
          'duration' => 'PT30S',
          'questionAnswers' => [],
        ],
        self::FAILURE_RESPONSE,
        [
          'challenge_uuid' => self::MISSING_UUID,
          'player_uuid' => '0b40135f-2458-46c9-a0ff-98ed04db7dac',
          'name' => 'Test player',
          'started' => $timestamp - 5600,
          'finished' => $timestamp - 4800,
          'points' => 0,
          'max_points' => 0,
          'tries_count' => 0,
          'user_id' => NULL,
          'data' => NULL,
        ]
      ],
      'inactive_challenge' => [
        [
          'contentId' => 1,
          'uuid' => 'feed763f-879e-49c8-ba5a-6330d1793881',
          'score' => 2,
          'maxScore' => 4,
          'duration' => 'PT30S',
          'questionAnswers' => [],
        ],
        self::NO_LONGER_ACTIVE_RESPONSE,
        [
          'challenge_uuid' => self::INACTIVE_CHALLENGE_UUID,
          'player_uuid' => 'feed763f-879e-49c8-ba5a-6330d1793881',
          'name' => 'Test player',
          'started' => $timestamp - 5600,
          'finished' => $timestamp - 4800,
          'points' => 0,
          'max_points' => 0,
          'tries_count' => 0,
          'user_id' => NULL,
          'data' => NULL,
        ]
      ],
    ];
  }

  /**
   * Test set finished action with incorrect data and expecting failures.
   *
   * @dataProvider provideIncorrectSetFinishedActionData
   * @param array $data
   * @param array $expectation
   * @param array|null $points_fields
   * @return void
   * @throws GuzzleException
   * @throws Exception
   */
  public function testSetFinishedActionFailures(array $data, array $expectation, ?array $points_fields = NULL): void {
    if ($points_fields) {
      $this->connection->insert('h5p_challenge_points')
        ->fields($points_fields)
        ->execute();
    }

    $response = $this->setFinished($data);
    $this->assertSame($expectation, $this->getJsonResponseData($response));
  }

  /**
   * Provide correct data for start playing action test.
   *
   * @return array[]
   */
  public static function provideCorrectSetFinishedActionData(): array {
    return [
      'correct_data' => [
        [
          'contentId' => 1,
          'uuid' => self::PLAYER_UUID,
          'score' => 2,
          'maxScore' => 4,
          'duration' => 'PT30S',
          'questionAnswers' => ['question' => 'answer'],
        ],
      ],
    ];
  }

  /**
   * Test set finished action with anonymous user and correct data and expecting success.
   *
   * @dataProvider provideCorrectSetFinishedActionData
   * @param array $data
   * @return void
   * @throws GuzzleException
   */
  public function testSuccessSetFinishedAction(array $data): void {
    $cookies = $this->getSessionCookies();
    $domain = parse_url($this->getUrl(), PHP_URL_HOST);
    $cookies->setCookie(new SetCookie([
      'Domain'  => $domain,
      'Name'    => 'Drupal.visitor.h5p-challenge-for-1',
      'Value'   => Json::encode([]),
      'Discard' => true
    ]));
    $response = $this->setFinished($data, $cookies);
    $this->assertSame([
      'success' => TRUE,
    ], $this->getJsonResponseData($response));

    $points = $this->challengeService->getChallengePointsByUuid(self::PLAYER_UUID);

    $this->assertNotNull($points);
    $this->assertSame(serialize([
      'duration' => 'PT30S',
      'answers' => ['question' => 'answer'],
    ]), $points->data);

    $cookie = $cookies->getCookieByName('Drupal.visitor.h5p-challenge-for-1');
    $this->assertNotNull($cookie, 'Challenge cookie not found');
    $this->assertSame(json_encode([
      'finished' => [
        'finished' => (int) $points->finished,
        'score' => (int) $points->points,
        'maxScore' => (int) $points->max_points,
      ],
    ]), urldecode($cookie->getValue()));
  }

  /**
   * Test challenge results page.
   *
   * @return void
   * @throws ElementNotFoundException
   * @throws ExpectationException
   */
  public function testResultsRoute(): void {
    $this->drupalGet('h5p_challenge/' . self::MISSING_UUID. '/results');
    $this->assertSession()->statusCodeEquals(404);

    $url = 'h5p_challenge/' . self::CHALLENGE_UUID. '/results';

    // TODO Challenge results created by authenticated users should be available to that user and administrator
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderContains('Cache-Control', 'no-cache');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'must-revalidate');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Challenge results: Test challenge"]');
    $this->assertSession()->elementExists('xpath', '//ul[contains(@class, "dropbutton")]/li/a[text() = "Download results as CSV"]');
    $this->assertSession()->elementExists('xpath', '//ul[contains(@class, "dropbutton")]/li/a[text() = "Target content"]');
    $this->assertSession()->elementExists('xpath', '//table[@class = "challenge-results"]/tbody/tr/td[text() = "Test player"]');

    // TODO Consider allowing administrator to both end and delete any challenges
    $this->drupalLogin($this->authenticatedUser);
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderContains('Cache-Control', 'no-cache');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'must-revalidate');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Challenge results: Test challenge"]');
    $this->assertSession()->elementExists('xpath', '//h2[text() = "Code: 123456"]');
    $this->assertSession()->elementExists('xpath', '//ul[contains(@class, "dropbutton")]/li/a[text() = "Download results as CSV"]');
    $this->assertSession()->elementExists('xpath', '//ul[contains(@class, "dropbutton")]/li/a[text() = "Target content"]');
    $this->assertSession()->elementExists('xpath', '//ul[contains(@class, "dropbutton")]/li/a[text() = "End challenge"]');
    $this->assertSession()->elementExists('xpath', '//ul[contains(@class, "dropbutton")]/li/a[text() = "Delete"]');
    $this->assertSession()->elementExists('xpath', '//table[@class = "challenge-results"]/tbody/tr/td[text() = "Test player"]');
  }

  /**
   * Test challenge results CSV route.
   *
   * @return void
   * @throws ExpectationException
   */
  public function testResultsCsvRoute(): void {
    $this->drupalGet('h5p_challenge/' . self::MISSING_UUID. '/results/csv');
    $this->assertSession()->statusCodeEquals(404);

    $url = 'h5p_challenge/' . self::CHALLENGE_UUID. '/results/csv';
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderContains('Cache-Control', 'no-cache');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'must-revalidate');
    $this->assertSession()->responseHeaderContains('Content-Type', 'text/csv');
    $this->assertSession()->responseHeaderEquals('Content-Disposition', 'attachment; filename="results.csv"');
  }

  /**
   * Test challenge reports list page.
   *
   * @return void
   * @throws ElementNotFoundException
   * @throws ExpectationException
   */
  public function testReportsListRoute(): void {
    $this->drupalGet('/admin/reports/h5p_challenge');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/reports/h5p_challenge');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderContains('Cache-Control', 'no-cache');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'must-revalidate');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Challenges"]');
    $this->assertSession()->elementExists('xpath', '//table/caption[text() = "Statistics"]');
    $this->assertSession()->elementExists('xpath', '//table/caption[text() = "Challenges"]');
    $this->assertSession()->elementExists('xpath', '//table[caption[text() = "Challenges"]]/tbody/tr[@class = "odd"]/td/a[text() = "Test challenge" and @href = "/h5p_challenge/' . self::CHALLENGE_UUID . '/results" and @target = "_blank"]');
  }

  /**
   * Test current user challenges list page.
   *
   * @return void
   * @throws ElementNotFoundException
   * @throws ExpectationException
   */
  public function testMineRoute(): void {
    $this->drupalGet('/h5p_challenge/mine');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->authenticatedUser);
    $this->drupalGet('/h5p_challenge/mine');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderContains('Cache-Control', 'no-cache');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'must-revalidate');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Challenges"]');
    $this->assertSession()->elementExists('xpath', '//table/tbody/tr[@id = "challenge-' . self::CHALLENGE_UUID . '" and @class = "odd"]/td/a[text() = "Test challenge" and @href = "/h5p_challenge/' . self::CHALLENGE_UUID . '/results"]');
  }

  /**
   * Test settings route.
   *
   * @return void
   * @throws ExpectationException
   */
  public function testSettingsRoute(): void {
    $response = $this->drupalGet('/h5p_challenge/settings.json');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderEquals('Content-Type', 'application/json');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'no-cache');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'must-revalidate');
    $this->assertSame(Json::encode($this->challengeService->getJsSettings()), $response);

    $this->drupalLogin($this->authenticatedUser);
    $response = $this->drupalGet('/h5p_challenge/settings.json');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderEquals('Content-Type', 'application/json');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'no-cache');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'must-revalidate');
    $this->assertSame(Json::encode($this->challengeService->getJsSettings()), $response);
  }

}
