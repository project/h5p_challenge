<?php

declare(strict_types=1);

namespace Drupal\Tests\h5p_challenge\Functional;

use Behat\Mink\Exception\ExpectationException;
use Behat\Mink\Exception\ResponseTextException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests access and functionality of challenge end form.
 *
 * @group h5p_challenge
 */
final class H5PChallengeEndFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['h5p_challenge'];

  protected $connection;
  protected UserInterface $ownerUser;
  protected string $endUrl;
  protected string $challengeUuid;

  /**
   * {@inheritdoc}
   * @throws EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    $this->connection = $this->container->get('database');
    $uuid = $this->container->get('uuid');

    $this->challengeUuid = $uuid->generate();
    $this->ownerUser = $this->createUser();

    $this->connection->insert('h5p_challenge')
      ->fields([
        'uuid' => $this->challengeUuid,
        'content_id' => 1,
        'title' => 'Test challenge',
        'email' => 'test@example.com',
        'started' => time(),
        'finished' => time() + 3600,
        'code' => '123456',
        'results_sent' => 0,
        'langcode' => 'en',
        'user_id' => NULL,
        'data' => NULL,
      ])
      ->execute();

    $this->endUrl = Url::fromRoute('h5p_challenge.h5p_challenge_end_form', [
      'challenge' => $this->challengeUuid,
    ])->toString();
  }

  /**
   * Updates preset challenge.
   *
   * @param array $fields
   * @return void
   */
  protected function updateChallenge(array $fields): void {
    $this->connection->update('h5p_challenge')
      ->fields($fields)
      ->condition('uuid', $this->challengeUuid)
      ->execute();
  }

  /**
   * Checks if preset challenge has ended.
   *
   * @return bool
   */
  protected function challengeHasEnded(): bool {
    return !!$this->connection->select('h5p_challenge', 'c')
      ->condition('c.uuid', $this->challengeUuid)
      ->condition('c.finished', time(), '<')
      ->countQuery()
      ->execute()
      ->fetchfield();
  }

  /**
   * Tests challenge end form access.
   *
   * @return void
   * @throws ExpectationException
   * @throws EntityStorageException
   */
  public function testAccess(): void {
    // Anonymous
    $this->drupalGet($this->endUrl);
    $this->assertSession()->statusCodeEquals(403);

    // Authenticated but not owner
    $this->drupalLogin($this->createUser());
    $this->drupalGet($this->endUrl);
    $this->assertSession()->statusCodeEquals(403);


    $this->updateChallenge([
      'user_id' => $this->ownerUser->id(),
    ]);

    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogout();

    // Owner
    $this->drupalLogin($this->ownerUser);
    $this->drupalGet($this->endUrl);
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests form cancel action.
   *
   * @return void
   * @throws ExpectationException
   */
  public function testCancel(): void {
    $results_url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_results', [
      'challenge' => $this->challengeUuid,
    ])->toString();

    $this->updateChallenge([
      'user_id' => $this->ownerUser->id(),
    ]);

    $this->drupalLogin($this->ownerUser);
    $this->drupalGet($this->endUrl);
    $page = $this->getSession()->getPage();
    $this->assertTrue($page->hasLink('Cancel'));
    $page->findLink('Cancel')->click();
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($results_url);
  }

  /**
   * Tests form end action.
   *
   * @return void
   * @throws ExpectationException
   * @throws ResponseTextException
   */
  public function testEnd(): void {
    $results_url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_results', [
      'challenge' => $this->challengeUuid,
    ])->toString();
    $this->drupalLogin($this->ownerUser);

    // Set challenge owner
    $this->updateChallenge([
      'user_id' => $this->ownerUser->id(),
    ]);

    // Make sure that ending challenge and redirection function as expected
    $this->drupalGet($this->endUrl);
    $this->assertSession()->pageTextContains('This will end the challenge and can not be undone!');
    $page = $this->getSession()->getPage();
    $page->findButton('End')->click();
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($results_url);
    $this->assertSession()->pageTextContains('Challenge ended.');
    sleep(1);
    $this->assertTrue($this->challengeHasEnded());

    // Make sure that ended or inactive challenges can not be ended again
    $this->drupalGet($this->endUrl);
    $page = $this->getSession()->getPage();
    $button = $page->findButton('End');
    $button->hasAttribute('disabled');
    $this->assertEquals('disabled', $button->getAttribute('disabled'));
  }

}
