<?php

declare(strict_types=1);

namespace Drupal\Tests\h5p_challenge\Functional;

use Behat\Mink\Exception\ElementNotFoundException;
use Behat\Mink\Exception\ExpectationException;
use Behat\Mink\Exception\ResponseTextException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Test description.
 *
 * @group h5p_challenge
 */
final class ConfigurationTest extends BrowserTestBase {

  const CONFIG_PATH = 'admin/config/system/h5p_challenge';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['h5p_challenge'];

  /**
   * Administrator account.
   *
   * @var UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   * @throws EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser(['access administration pages']);
  }

  /**
   * Test config page access with anonymous and admin user. Make sure that all fields exist and have correct values.
   *
   * @return void
   * @throws ExpectationException
   */
  public function testConfigurationForm(): void {
    $this->drupalGet(self::CONFIG_PATH);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet(self::CONFIG_PATH);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "H5P Challenge Settings"]');

    $this->assertSession()->fieldExists('recaptcha_site_key');
    $this->assertSession()->fieldValueEquals('recaptcha_site_key', '');

    $this->assertSession()->fieldExists('recaptcha_secret_key');
    $this->assertSession()->fieldValueEquals('recaptcha_secret_key', '');

    $this->assertSession()->fieldExists('email_from_address');
    $this->assertSession()->fieldValueEquals('email_from_address', '');
  }

  /**
   * Test configuration page and make sure it can be saved.
   *
   * @return void
   * @throws ElementNotFoundException
   * @throws ExpectationException
   * @throws ResponseTextException
   */
  public function testConfigurationFormSubmit(): void {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet(self::CONFIG_PATH);
    $page = $this->getSession()->getPage();
    $page->fillField('recaptcha_site_key', 'public key');
    $page->fillField('recaptcha_secret_key', 'secret key');
    $page->fillField('email_from_address', 'no-reply@example.com');
    $page->findButton('Save configuration')->click();
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    $this->drupalGet(self::CONFIG_PATH);
    $this->assertSession()->fieldValueEquals('recaptcha_site_key', 'public key');
    $this->assertSession()->fieldValueEquals('recaptcha_secret_key', 'secret key');
    $this->assertSession()->fieldValueEquals('email_from_address', 'no-reply@example.com');
  }

}
