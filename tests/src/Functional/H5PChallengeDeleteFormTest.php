<?php

declare(strict_types=1);

namespace Drupal\Tests\h5p_challenge\FunctionalJavascript;

use Behat\Mink\Exception\ExpectationException;
use Behat\Mink\Exception\ResponseTextException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests access and functionality of challenge delete form.
 *
 * @group h5p_challenge
 */
final class H5PChallengeDeleteFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['h5p_challenge'];

  protected $connection;
  protected UserInterface $ownerUser;
  protected string $challengeUuid;
  protected string $deleteUrl;

  /**
   * {@inheritdoc}
   * @throws EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    $this->connection = $this->container->get('database');
    $uuid = $this->container->get('uuid');

    $this->challengeUuid = $uuid->generate();
    $this->ownerUser = $this->createUser();

    $this->connection->insert('h5p_challenge')
      ->fields([
        'uuid' => $this->challengeUuid,
        'content_id' => 1,
        'title' => 'Test challenge',
        'email' => 'test@example.com',
        'started' => time(),
        'finished' => time() + 3600,
        'code' => '123456',
        'results_sent' => 0,
        'langcode' => 'en',
        'user_id' => NULL,
        'data' => NULL,
      ])
      ->execute();

    $this->deleteUrl = Url::fromRoute('h5p_challenge.h5p_challenge_delete_form', [
      'challenge' => $this->challengeUuid,
    ])->toString();
  }

  /**
   * Updates preset challenge.
   *
   * @param array $fields
   * @return void
   */
  protected function updateChallenge(array $fields): void {
    $this->connection->update('h5p_challenge')
      ->fields($fields)
      ->condition('uuid', $this->challengeUuid)
      ->execute();
  }

  /**
   * Checks if preset challenge still exists.
   *
   * @return bool
   */
  protected function challengeExists(): bool {
    return !!$this->connection->select('h5p_challenge', 'c')
      ->condition('c.uuid', $this->challengeUuid)
      ->countQuery()
      ->execute()
      ->fetchfield();
  }

  /**
   * Tests challenge delete form access.
   *
   * @return void
   * @throws ExpectationException
   * @throws EntityStorageException
   */
  public function testAccess(): void {
    // Anonymous
    $this->drupalGet($this->deleteUrl);
    $this->assertSession()->statusCodeEquals(403);

    // Authenticated but not owner
    $this->drupalLogin($this->createUser());
    $this->drupalGet($this->deleteUrl);
    $this->assertSession()->statusCodeEquals(403);


    $this->updateChallenge([
      'user_id' => $this->ownerUser->id(),
    ]);

    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogout();

    // Owner
    $this->drupalLogin($this->ownerUser);
    $this->drupalGet($this->deleteUrl);
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests form cancel action.
   *
   * @return void
   * @throws ExpectationException
   */
  public function testCancel(): void {
    $results_url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_results', [
      'challenge' => $this->challengeUuid,
    ])->toString();

    $this->updateChallenge([
      'user_id' => $this->ownerUser->id(),
    ]);

    $this->drupalLogin($this->ownerUser);
    $this->drupalGet($this->deleteUrl);
    $page = $this->getSession()->getPage();
    $this->assertTrue($page->hasLink('Cancel'));
    $page->findLink('Cancel')->click();
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($results_url);
  }

  /**
   * Tests form delete action.
   *
   * @return void
   * @throws ExpectationException
   * @throws ResponseTextException
   */
  public function testDelete(): void {
    $mine_url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_mine')
      ->toString();
    $this->drupalLogin($this->ownerUser);

    // Set challenge owner
    $this->updateChallenge([
      'user_id' => $this->ownerUser->id(),
    ]);

    // Make sure that unfinished challenge with unsent results can not be deleted
    $this->drupalGet($this->deleteUrl);
    $page = $this->getSession()->getPage();
    $button = $page->findButton('Delete');
    $button->hasAttribute('disabled');
    $this->assertEquals('disabled', $button->getAttribute('disabled'));
    $this->assertSession()->pageTextContains('Challenge can not be removed for as long as it is active and results have not been sent yet.');

    // Set challenge as finished
    $this->updateChallenge([
      'finished' => time() - 1,
    ]);

    // Make sure that challenge with unsent results can not be deleted
    $this->drupalGet($this->deleteUrl);
    $page = $this->getSession()->getPage();
    $button = $page->findButton('Delete');
    $button->hasAttribute('disabled');
    $this->assertEquals('disabled', $button->getAttribute('disabled'));
    $this->assertSession()->pageTextContains('Challenge can not be removed for as long as it is active and results have not been sent yet.');

    // Set results as sent
    $this->updateChallenge([
      'results_sent' => 1,
    ]);

    // Make sure that challenge deletion and redirection function as expected
    $this->drupalGet($this->deleteUrl);
    $this->assertSession()->pageTextContains('This will remove the challenge itself and all of its player responses. This can not be undone!');
    $this->assertSession()->pageTextNotContains('Challenge can not be removed for as long as it is active and results have not been sent yet.');
    $page = $this->getSession()->getPage();
    $page->findButton('Delete')->click();
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($mine_url);
    $this->assertSession()->pageTextContains('Challenge removed.');
    $this->assertFalse($this->challengeExists());
  }

}
