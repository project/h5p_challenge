<?php

declare(strict_types=1);

namespace Drupal\Tests\h5p_challenge\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\h5p\Entity\H5PContent;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;
use Drupal\Tests\UnitTestCase;

/**
 * Test H5PChallenge class.
 *
 * @group h5p_challenge
 */
final class H5PChallengeTest extends UnitTestCase {

  protected H5PChallenge $challenge;
  protected ContainerBuilder $container;
  protected H5PContent $content;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->challenge = new H5PChallenge();

    $this->content = $this->createMock(H5PContent::class);
    $this->content
      ->expects($this->any())
      ->method('get')
      ->willReturn((object) ['value' => 'Content title']);
    $storage = $this->createMock(EntityStorageInterface::class);
    $storage
      ->expects($this->any())
      ->method('load')
      ->willReturnCallback(fn(?int $id) => $id === 1 ? $this->content : NULL);
    $manager = $this->createMock(EntityTypeManagerInterface::class);
    $manager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);
    $repository = $this->createMock(EntityTypeRepositoryInterface::class);

    $this->container = new ContainerBuilder();
    $this->container->set('entity_type.repository', $repository);
    $this->container->set('entity_type.manager', $manager);
    \Drupal::setContainer($this->container);
  }

  /**
   * Tests that correct content types with scores are defined.
   *
   * @return void
   */
  public function testConstants(): void {
    self::assertEquals([
      'H5P.ImageHotspotQuestion',
      'H5P.Blanks',
      'H5P.Essay',
      'H5P.SingleChoiceSet',
      'H5P.MultiChoice',
      'H5P.TrueFalse',
      'H5P.DragQuestion',
      'H5P.Summary',
      'H5P.DragText',
      'H5P.MarkTheWords',
      'H5P.MemoryGame',
      'H5P.QuestionSet',
      'H5P.InteractiveVideo',
      'H5P.CoursePresentation',
      'H5P.DocumentationTool',
      'H5P.MultiMediaChoice',
    ], $this->challenge::TYPES_WITH_SCORE);
  }

  /**
   * Test getting content title by identifier.
   *
   * @return void
   */
  public function testGetContentTitleById(): void {
    self::assertEquals('Content title', $this->challenge->getContentTitleById(1));
    self::assertEquals('', $this->challenge->getContentTitleById(2));
  }

  /**
   * Provides data for get content questions test.
   *
   * @return array
   */
  public static function provideTestGetContentQuestionsData(): array {
    return [
      'empty_data_and_empty_expectation' => [
        [],
        [],
      ],
      'unsuitable_library_data_and_empty_expectation' => [
        [
          'subContentId' => 1,
          'library' => 'H5P.Test 1.1.1',
          'metadata' => (object) [
            'title' => 'H5P.Test title',
          ],
        ],
        [],
      ],
      'non_empty_data_and_non_empty_expectation' => [
        [
          // Library type not suitable for tasks
          [
            'subContentId' => 1,
            'library' => 'H5P.Test 1.1.1',
            'metadata' => (object) [
              'title' => 'H5P.Test title',
            ],
          ],
          [
            'subContentId' => 2,
            'library' => 'H5P.ImageHotspotQuestion 1.1.1',
            'metadata' => (object) [
              'title' => 'H5P.ImageHotspotQuestion title',
            ],
          ],
          [
            [
              'subContentId' => 3,
              'library' => 'H5P.TrueFalse 1.1.1',
              'metadata' => (object) [
                'title' => 'H5P.TrueFalse title',
              ],
            ],
            [
              [
                'subContentId' => 4,
                'library' => 'H5P.Summary 1.1.1',
                'metadata' => (object) [
                  'title' => 'H5P.Summary title',
                ],
              ],
              [
                // Depth is above three
                [
                  'subContentId' => 5,
                  'library' => 'H5P.Blanks 1.1.1',
                  'metadata' => (object) [
                    'title' => 'H5P.Blanks title',
                  ],
                ],
              ]
            ]
          ],
        ],
        [
          2 => [
            'id' => 2,
            'type' => 'H5P.ImageHotspotQuestion 1.1.1',
            'title' => 'H5P.ImageHotspotQuestion title',
          ],
          3 => [
            'id' => 3,
            'type' => 'H5P.TrueFalse 1.1.1',
            'title' => 'H5P.TrueFalse title',
          ],
          4 => [
            'id' => 4,
            'type' => 'H5P.Summary 1.1.1',
            'title' => 'H5P.Summary title',
          ],
        ]
      ],
    ];
  }

  /**
   * Tests getting content questions.
   *
   * @dataProvider provideTestGetContentQuestionsData
   * @param array $parameters
   * @param array $expectation
   * @return void
   */
  public function testGetContentQuestions(array $parameters, array $expectation): void {
    $content = $this->createMock(H5PContent::class);
    $content
      ->expects($this->any())
      ->method('getFilteredParameters')
      ->willReturn(json_encode($parameters));
    self::assertEquals($expectation, $this->challenge->getContentQuestions($content));
  }

  /**
   * Tests checking content active status.
   */
  public function testIsActive(): void {
    $this->challenge->finished = time() - 3600;
    self::assertFalse($this->challenge->isActive());

    $this->challenge->finished = time() + 3600;
    self::assertTrue($this->challenge->isActive());
  }

  /**
   * Tests getting content title.
   *
   * @return void
   */
  public function testGetContentTitle(): void {
    $this->challenge->content_id = 1;
    self::assertEquals('Content title', $this->challenge->getContentTitle());

    $this->challenge->content_id = 2;
    self::assertEquals('', $this->challenge->getContentTitle());

    $this->challenge->data = serialize([
      'content' => [
        'title' => 'Stored title',
      ],
    ]);
    self::assertEquals('Stored title', $this->challenge->getContentTitle());

    $this->challenge->content_id = 1;
    self::assertEquals('Content title', $this->challenge->getContentTitle());
  }

  /**
   * Provides data for get questions data test.
   *
   * @return array
   */
  public static function provideTestGetQuestionsData(): array {
    return [
      'null' => [
        NULL,
        [],
      ],
      'empty_array' => [
        [],
        [],
      ],
      'incorrect_data' => [
        [
          'content' => [
            'questions' => 'incorrect data type',
          ],
        ],
        [],
      ],
      'no_questions' => [
        [
          'content' => [
            'questions' => [],
          ],
        ],
        [],
      ],
      'has_questions' => [
        [
          'content' => [
            'questions' => [
              1 => [
                'id' => 1,
                'title' => 'Question title',
              ]
            ],
          ],
        ],
        [
          1 => [
            'id' => 1,
            'title' => 'Question title',
          ]
        ],
      ],
    ];
  }

  /**
   * Tests getting questions.
   *
   * @dataProvider provideTestGetQuestionsData
   * @param array|null $data
   * @param array $expectation
   * @return void
   */
  public function testGetQuestions(?array $data, array $expectation): void {
    $this->challenge->data = is_array($data) ? serialize($data) : $data;
    self::assertEquals($expectation, $this->challenge->getQuestions());
  }

  /**
   * Provides data for get question identifiers test.
   *
   * @return array
   */
  public static function provideTestGetQuestionIdsData(): array {
    return [
      'null' => [
        NULL,
        [],
      ],
      'empty_array' => [
        [],
        [],
      ],
      'has_data' => [
        [
          'content' => [
            'questions' => [
              1 => [
                'id' => 1,
                'title' => 'Question title',
              ]
            ],
          ],
        ],
        [1],
      ],
    ];
  }

  /**
   * Test getting question identifiers.
   *
   * @dataProvider provideTestGetQuestionIdsData
   * @param array|null $data
   * @param array $expectation
   * @return void
   */
  public function testGetQuestionIds(?array $data, array $expectation): void {
    $this->challenge->data = is_array($data) ? serialize($data) : $data;
    self::assertEquals($expectation, $this->challenge->getQuestionIds());

  }

  /**
   * Tests belonging to user.
   *
   * @return void
   */
  public function testBelongsToUser(): void {
    $this->challenge->user_id = NULL;
    self::assertFalse($this->challenge->belongsToUser());

    $this->challenge->user_id = 0;
    self::assertFalse($this->challenge->belongsToUser());

    $this->challenge->user_id = 1;
    self::assertTrue($this->challenge->belongsToUser());
  }

  /**
   * Tests getting content entity.
   *
   * @return void
   */
  public function testGetContentEntity(): void {
    $this->challenge->content_id = 1;
    self::assertSame($this->content, $this->challenge->getContentEntity());

    $this->challenge->content_id = 2;
    self::assertNull($this->challenge->getContentEntity());
  }

}
