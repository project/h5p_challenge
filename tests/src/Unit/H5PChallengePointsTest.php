<?php

declare(strict_types=1);

namespace Drupal\Tests\h5p_challenge\Unit;

use Drupal\h5p_challenge\FetchClass\H5PChallengePoints;
use Drupal\Tests\UnitTestCase;

/**
 * Tests H5PChallengePoints class.
 *
 * @group h5p_challenge
 */
final class H5PChallengePointsTest extends UnitTestCase {

  protected H5PChallengePoints $points;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->points = new H5PChallengePoints();
  }

  /**
   * Provides data for get answers test.
   *
   * @return array[]
   */
  public static function provideGetAnswersData(): array {
    return [
      'null_and_empty_array' => [
        NULL,
        [],
      ],
      'empty_and_empty_array' => [
        '',
        [],
      ],
      'empty_array_and_empty_array' => [
        serialize([]),
        [],
      ],
      'incorrect_answers_value_and_empty_array' => [
        serialize([
          'answers' => 'incorrect value',
        ]),
        [],
      ],
      'correct_data_and_correct_array' => [
        serialize([
          'answers' => [
            1 => [
              'id' => 1,
              'title' => 'Title',
            ]
          ],
        ]),
        [
          1 => [
            'id' => 1,
            'title' => 'Title',
          ],
        ],
      ],
    ];
  }

  /**
   * Tests getting answers.
   *
   * @dataProvider provideGetAnswersData
   */
  public function testGetAnswers(?string $data, array $expected): void {
    $this->points->data = $data;
    self::assertEquals($expected, $this->points->getAnswers());
  }

  /**
   * Provides data for checking if all provided question identifiers have been answered test.
   *
   * @return array[]
   */
  public static function provideTestHasAllQuestionsAnsweredData(): array {
    return [
      'null_and_empty_value_and_true' => [
        NULL,
        [],
        TRUE,
      ],
      'empty_and_empty_value_and_true' => [
        '',
        [],
        TRUE,
      ],
      'empty_array_and_empty_value_and_true' => [
        serialize([
          'answers' => [],
        ]),
        [],
        TRUE,
      ],
      'empty_array_and_one_question_and_false' => [
        serialize([
          'answers' => [],
        ]),
        [1],
        FALSE,
      ],
      'one_answer_and_one_question_and_true' => [
        serialize([
          'answers' => [
            1 => [
              'id' => 1,
              'title' => 'Title',
            ],
          ],
        ]),
        [1],
        TRUE,
      ],
      'two_answers_and_one_question_and_true' => [
        serialize([
          'answers' => [
            1 => [
              'id' => 1,
              'title' => 'Title',
            ],
            2 => [
              'id' => 2,
              'title' => 'Title 2',
            ],
          ],
        ]),
        [1],
        TRUE,
      ],
      'two_answers_and_two_questions_and_true' => [
        serialize([
          'answers' => [
            1 => [
              'id' => 1,
              'title' => 'Title',
            ],
            2 => [
              'id' => 2,
              'title' => 'Title 2',
            ],
          ],
        ]),
        [1, 2],
        TRUE,
      ],
      'two_answers_and_three_questions_and_false' => [
        serialize([
          'answers' => [
            1 => [
              'id' => 1,
              'title' => 'Title',
            ],
            2 => [
              'id' => 2,
              'title' => 'Title 2',
            ],
          ],
        ]),
        [1, 2, 3],
        FALSE,
      ],
    ];
  }

  /**
   * Test checking if all provided question identifiers have been answered.
   *
   * @dataProvider provideTestHasAllQuestionsAnsweredData
   * @param string|null $data
   * @param array $identifiers
   * @param bool $expected
   * @return void
   */
  public function testHasAllQuestionsAnswered(?string $data, array $identifiers, bool $expected): void {
    $this->points->data = $data;
    self::assertEquals($expected, $this->points->hasAllQuestionsAnswered($identifiers));
  }

}
