<?php

declare(strict_types=1);

namespace Drupal\Tests\h5p_challenge\Kernel;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Delete;
use Drupal\Core\Database\Query\Insert;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Database\Query\Update;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailFormatHelper;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\h5p\Entity\H5PContent;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;
use Drupal\h5p_challenge\FetchClass\H5PChallengePoints;
use Drupal\h5p_challenge\H5PChallengeService;
use Drupal\h5p_challenge\H5PChallengeServiceInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Exception;
use PHPUnit\Framework\Constraint\IsType;

/**
 * Test description.
 *
 * @group h5p_challenge
 */
final class H5PChallengeServiceTest extends KernelTestBase {

  use UserCreationTrait;

  const TIMESTAMP = 1719428873;
  const CHALLENGE_UUID = '946fc6a7-1e07-4fb1-a4b9-d96f28bc5578';
  const PLAYER_UUID = 'fb108cce-b29c-4958-8605-40f74fffc301';
  const MISSING_UUID = 'b717e213-6d32-4211-a444-8823f58267c0';
  const DATE_FORMAT_PATTERN = 'l, F j, Y - H:i';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'user', 'h5p', 'h5p_challenge'];

  /**
   * Database connection.
   *
   * @var Connection
   */
  protected Connection $connection;

  /**
   * H5PChallenge service.
   *
   * @var H5PChallengeServiceInterface
   */
  protected H5PChallengeServiceInterface $service;

  /**
   * H5PChallenge config.
   *
   * @var Config
   */
  protected Config $config;

  /**
   * {@inheritdoc}
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('h5p_challenge', [
      'h5p_challenge',
      'h5p_challenge_points',
    ]);
    $this->installEntitySchema('h5p_content');

    $this->connection = $this->container->get('database');
    $this->service = $this->container->get('h5p.challenge.default');
    $this->config = $this->config('h5p_challenge.config');

    $entity = H5PContent::create([
      'library_id' => 1,
      'parameters' => '',
      'title' => 'Test content',
    ]);
    $entity->save();

    $this->connection->insert('h5p_challenge')
      ->fields([
        'uuid' => self::CHALLENGE_UUID,
        'content_id' => 1,
        'title' => 'Test challenge',
        'email' => 'test@example.com',
        'started' => self::TIMESTAMP,
        'finished' => self::TIMESTAMP + 3600,
        'code' => '123456',
        'results_sent' => 0,
        'langcode' => 'en',
        'user_id' => 1,
        'data' => serialize([
          'content' => [
            'questions' => [
              [
                'id' => 1,
                'title' => 'First question',
              ],
              [
                'id' => 2,
                'title' => 'Second question',
              ],
            ],
          ],
        ]),
      ])
      ->execute();

    $this->connection->insert('h5p_challenge_points')
      ->fields([
        'challenge_uuid' => self::CHALLENGE_UUID,
        'player_uuid' => self::PLAYER_UUID,
        'name' => 'Test player',
        'started' => self::TIMESTAMP + 600,
        'finished' => self::TIMESTAMP + 1200,
        'points' => 0,
        'max_points' => 0,
        'tries_count' => 0,
        'user_id' => NULL,
        'data' => serialize([
          'answers' => [
            1 => [
              'score' => 2,
              'maxScore' => 4,
            ],
          ],
        ]),
      ])
      ->execute();
  }

  /**
   * Returns file content from test data directory.
   *
   * @param string $file_name
   * @return string
   * @throws Exception
   */
  public function getDataFileContent(string $file_name): string {
    $path = __DIR__ . '/data/' . $file_name;

    if (!(file_exists($path) && is_file($path))) {
      throw new Exception(sprintf('File %s does not exist.', $path));
    }

    return file_get_contents($path);
  }

  /**
   * Tests methods that return configuration values.
   *
   * @return void
   */
  public function testConfiguration(): void {
    self::assertNull($this->service->getRecaptchaSiteKey());
    self::assertNull($this->service->getRecaptchaSecretKey());
    self::assertNull($this->service->getEmailFromAddress());

    $this->config
      ->set('recaptcha_site_key', 'public key')
      ->set('recaptcha_secret_key', 'secret key')
      ->set('email_from_address', 'email@example.com')
      ->save();

    self::assertEquals('public key', $this->service->getRecaptchaSiteKey());
    self::assertEquals('secret key', $this->service->getRecaptchaSecretKey());
    self::assertEquals('email@example.com', $this->service->getEmailFromAddress());
  }

  /**
   * Tests random code generation.
   *
   * @return void
   */
  public function testGenerateRandomNumber(): void {
    $value = $this->service->generateRandomNumber(6);
    self::assertEquals('6', strlen($value));
    self::assertMatchesRegularExpression('/^[0-9]+$/', $value);
  }

  /**
   * Tests unique challenge code check.
   *
   * @return void
   */
  public function testIsUniqueChallengeCode(): void {
    self::assertFalse($this->service->isUniqueChallengeCode('123456', 1));
    self::assertTrue($this->service->isUniqueChallengeCode('654321', 1));
    self::assertTrue($this->service->isUniqueChallengeCode('123456', 2));
  }

  /**
   * Tests unique challenge code generation for H5PContent.
   *
   * @return void
   * @throws Exception
   */
  public function testGenerateUniqueChallengeCode(): void {
    $code = $this->service->generateUniqueChallengeCode(1);
    self::assertMatchesRegularExpression('/^[0-9]+$/', $code);
    self::assertNotEquals('123456', $code);

    $code = $this->service->generateUniqueChallengeCode(2);
    self::assertMatchesRegularExpression('/^[0-9]+$/', $code);

    // Use partial mock to test behaviour of unique challenge code generator when four initially generated codes are
    // not unique.
    $service = $this->createPartialMock(H5PChallengeService::class, ['isUniqueChallengeCode']);
    $service->expects($this->exactly(5))
      ->method('isUniqueChallengeCode')
      ->willReturn(FALSE, FALSE, FALSE, FALSE, TRUE);
    $code = $service->generateUniqueChallengeCode(3);
    self::assertMatchesRegularExpression('/^[0-9]+$/', $code);
    self::assertEquals(7, strlen($code));
  }

  /**
   * Tests content existence check.
   *
   * @return void
   */
  public function testCheckIfContentExists(): void {
    self::assertTrue($this->service->checkIfContentExists(1));
    self::assertFalse($this->service->checkIfContentExists(2));
  }

  /**
   * Test loading challenge by content id and code.
   *
   * @return void
   */
  public function testGetChallengeByContentIdAndCode(): void {
    self::assertInstanceOf(H5PChallenge::class, $this->service->getChallengeByContentIdAndCode(1, '123456'));
    self::assertNull($this->service->getChallengeByContentIdAndCode(1, '654321'));
    self::assertNull($this->service->getChallengeByContentIdAndCode(2, '123456'));
  }

  /**
   * Test loading challenge points by player UUID.
   *
   * @return void
   */
  public function testGetChallengePointsByUuid(): void {
    self::assertInstanceOf(H5PChallengePoints::class, $this->service->getChallengePointsByUuid(self::PLAYER_UUID));
    self::assertNull($this->service->getChallengePointsByUuid(self::MISSING_UUID));
  }

  /**
   * Test loading challenge by UUID.
   *
   * @return void
   */
  public function testGetChallengeByUuid(): void {
    self::assertInstanceOf(H5PChallenge::class, $this->service->getChallengeByUuid(self::CHALLENGE_UUID));
    self::assertNull($this->service->getChallengeByUuid(self::MISSING_UUID));
  }

  /**
   * Tests challenge results CSV generation.
   *
   * @return void
   *
   * @throws EntityStorageException
   * @throws Exception
   */
  public function testGetChallengeResultsCsv(): void {
    $challenge = $this->service->getChallengeByUuid(self::CHALLENGE_UUID);

    // TODO Service would need to make sure that the format is present as an error is thrown if it is not
    DateFormat::create([
      'id' => 'long',
      'label' => 'Default long date',
      'pattern' => self::DATE_FORMAT_PATTERN,
    ])->save();

    self::assertEquals($this->getDataFileContent('results_with_data.csv'), $this->service->getChallengeResultsCsv($challenge));
  }

  /**
   * Provides data for notifications cron test.
   *
   * @return array[]
   */
  public function provideTestNotificationsCronData(): array {
    return [
      'authenticated' => [1, 'authenticated_notification_email_template.txt'],
      'anonymous' => [NULL, 'anonymous_notification_email_template.txt'],
    ];
  }

  /**
   * Tests notifications cron.
   *
   * @dataProvider provideTestNotificationsCronData
   * @param int|null $user_id
   * @param string $notification_template_file_name
   * @return void
   * @throws Exception
   */
  public function testNotificationsCron(?int $user_id, string $notification_template_file_name): void {
    $challenge = new H5PChallenge();
    $challenge->uuid = '1d58e145-dea0-415a-a8c6-b23bd221c2d0';
    $challenge->content_id = 1;
    $challenge->started = time();
    $challenge->finished = time() + 3600;
    $challenge->user_id = $user_id;
    $challenge->title = 'Test challenge';
    $challenge->langcode = 'en';
    $challenge->email = 'test@example.com';
    $challenge_statement = $this->createMock(StatementInterface::class);
    $challenge_statement
      ->expects($this->exactly(2))
      ->method('fetch')
      ->willReturn($challenge, NULL);
    $challenge_select = $this->createMock(SelectInterface::class);
    $challenge_select
      ->expects($this->once())
      ->method('fields')
      ->with('c')
      ->willReturnSelf();
    $challenge_select
      ->expects($this->exactly(2))
      ->method('condition')
      ->willReturnCallback(function($field, $value, $operator) use ($challenge_select) {
        $is_results_sent = $field === 'c.results_sent' && $value === 0 && $operator === '=';
        $is_finished = $field === 'c.finished' && is_int($value) && $operator === '<';

        self::assertTrue($is_results_sent || $is_finished, sprintf('Unrecognised argument combination passed to method condition(%s, %s, %s)', $field, $value, $operator));

        return $challenge_select;
      });
    $challenge_select
      ->expects($this->once())
      ->method('execute')
      ->willReturn($challenge_statement);
    $points_statement = $this->createMock(StatementInterface::class);
    $points_statement
      ->expects($this->once())
      ->method('fetchField')
      ->willReturn(5);
    $points_select = $this->createMock(SelectInterface::class);
    $points_select
      ->expects($this->once())
      ->method('condition')
      ->with('cp.challenge_uuid', $challenge->uuid)
      ->willReturnSelf();
    $points_select
      ->expects($this->once())
      ->method('addExpression')
      ->with('COUNT(*)');
    $points_select
      ->expects($this->once())
      ->method('countQuery');
    $points_select
      ->expects($this->once())
      ->method('execute')
      ->willReturn($points_statement);
    $points_statement_csv = $this->createMock(StatementInterface::class);
    $points_statement_csv
      ->expects($this->once())
      ->method('fetch')
      ->willReturn(NULL);
    $points_select_csv = $this->createMock(SelectInterface::class);
    $points_select_csv
      ->expects($this->once())
      ->method('fields')
      ->with('cp')
      ->willReturnSelf();
    $points_select_csv
      ->expects($this->once())
      ->method('condition')
      ->with('cp.challenge_uuid', $challenge->uuid)
      ->willReturnSelf();
    $points_select_csv
      ->expects($this->once())
      ->method('orderBy')
      ->with('cp.started')
      ->willReturnSelf();
    $points_select_csv
      ->expects($this->once())
      ->method('execute')
      ->willReturn($points_statement_csv);
    $update = $this->createMock(Update::class);
    $update
      ->expects($this->once())
      ->method('condition')
      ->with('uuid', $challenge->uuid)
      ->willReturnSelf();
    $update
      ->expects($this->once())
      ->method('fields')
      ->with([
        'results_sent' => 1,
      ])
      ->willReturnSelf();
    $update
      ->expects($this->once())
      ->method('execute');
    $insert = $this->createMock(Insert::class);
    $insert
      ->expects($this->once())
      ->method('fields')
      ->with([
        'content_id' => $challenge->content_id,
        'started' => $challenge->started,
        'finished' => $challenge->finished,
        'player_count' => 5,
      ])
      ->willReturnSelf();
    $insert
      ->expects($this->once())
      ->method('execute');
    $connection = $this->createMock(Connection::class);
    $connection
      ->expects($this->exactly(3))
      ->method('select')
      ->willReturnMap([
        ['h5p_challenge_points', 'cp', [], $points_select],
        ['h5p_challenge', 'c', [
          'fetch' => H5PChallenge::class,
        ], $challenge_select],
        ['h5p_challenge_points', 'cp', [
          'fetch' => H5PChallengePoints::class,
        ], $points_select_csv],
      ]);
    $connection
      ->expects($this->once())
      ->method('update')
      ->with('h5p_challenge')
      ->willReturn($update);
    $connection
      ->expects($this->once())
      ->method('insert')
      ->with('h5p_challenge_statistics')
      ->willReturn($insert);
    $date_formatter = $this->createMock(DateFormatterInterface::class);
    $date_formatter
      ->expects($this->exactly(2))
      ->method('format')
      ->willReturnCallback(fn (int $value, string $format) => date(self::DATE_FORMAT_PATTERN, $value));
    $challenge_results_url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_results', [
      'challenge' => $challenge->uuid,
    ], [
      'absolute' => TRUE,
    ])->toString();
    $challenge_results_csv_url = Url::fromRoute('h5p_challenge.h5p_challenge_controller_results_csv', [
      'challenge' => $challenge->uuid,
    ], [
      'absolute' => TRUE,
    ])->toString();
    $email_body = sprintf($this->getDataFileContent($notification_template_file_name), date(self::DATE_FORMAT_PATTERN, $challenge->started), date(self::DATE_FORMAT_PATTERN, $challenge->finished), $challenge_results_url, $challenge_results_csv_url);
    $params = [
      'body' => $email_body,
      'plaintext' => MailFormatHelper::wrapMail($email_body),
      'attachment' => [
        'filecontent' => $this->getDataFileContent('results_with_just_header.csv'),
        'filename' => 'results.csv',
        'filemime' => 'text/csv',
      ],
    ];
    $mail_manager = $this->createMock(MailManagerInterface::class);
    $mail_manager
      ->expects($this->once())
      ->method('mail')
      ->with('h5p_challenge', 'challenge_ended', $challenge->email, $challenge->langcode, $params);
    $service = new H5PChallengeService(
      $connection,
      $this->createMock(ConfigFactoryInterface::class),
      $date_formatter,
      $mail_manager,
      $this->createMock(AccountProxyInterface::class),
      $this->createMock(LanguageManagerInterface::class)
    );

    $service->notificationsCron();
  }

  /**
   * Tests cleanup cron.
   *
   * @return void
   * @throws Exception
   */
  public function testCleanupCron(): void {
    $identifiers = [1, 2, 3];
    $statement = $this->createMock(StatementInterface::class);
    $statement
      ->expects($this->once())
      ->method('fetchCol')
      ->with(0)
      ->willReturn($identifiers);
    $select = $this->createMock(SelectInterface::class);
    $select
      ->expects($this->once())
      ->method('fields')
      ->with('c', ['uuid'])
      ->willReturnSelf();
    $select
      ->expects($this->once())
      ->method('condition')
      ->with('c.finished', new IsType(IsType::TYPE_INT), '<')
      ->willReturnSelf();
    $select
      ->expects($this->once())
      ->method('isNull')
      ->with('c.user_id')
      ->willReturnSelf();
    $select
      ->expects($this->once())
      ->method('execute')
      ->willReturn($statement);
    $delete_points = $this->createMock(Delete::class);
    $delete_points
      ->expects($this->once())
      ->method('condition')
      ->with('challenge_uuid', $identifiers, 'IN')
      ->willReturnSelf();
    $delete_points
      ->expects($this->once())
      ->method('execute');
    $delete_challenges = $this->createMock(Delete::class);
    $delete_challenges
      ->expects($this->once())
      ->method('condition')
      ->with('uuid', $identifiers, 'IN')
      ->willReturnSelf();
    $delete_challenges
      ->expects($this->once())
      ->method('execute');
    $update = $this->createMock(Update::class);
    $update
      ->expects($this->once())
      ->method('condition')
      ->with('finished', new IsType(IsType::TYPE_INT), '<')
      ->willReturnSelf();
    $update
      ->expects($this->once())
      ->method('isNotNull')
      ->with('user_id')
      ->willReturnSelf();
    $update
      ->expects($this->once())
      ->method('fields')
      ->with([
        'code' => NULL,
      ])
      ->willReturnSelf();
    $update
      ->expects($this->once())
      ->method('execute');
    $connection = $this->createMock(Connection::class);
    $connection
      ->expects($this->once())
      ->method('select')
      ->with('h5p_challenge', 'c')
      ->willReturn($select);
    $connection
      ->expects($this->exactly(2))
      ->method('delete')
      ->willReturnMap([
        ['h5p_challenge_points', [], $delete_points],
        ['h5p_challenge', [], $delete_challenges],
      ]);
    $connection
      ->expects($this->once())
      ->method('update')
      ->with('h5p_challenge')
      ->willReturn($update);
    $service = new H5PChallengeService(
      $connection,
      $this->createMock(ConfigFactoryInterface::class),
      $this->createMock(DateFormatterInterface::class),
      $this->createMock(MailManagerInterface::class),
      $this->createMock(AccountProxyInterface::class),
      $this->createMock(LanguageManagerInterface::class)
    );
    $service->cleanupCron();
  }

  /**
   * Tests localization settings generation.
   *
   * @return void
   */
  public function testGetLocalization(): void {
    $localization = $this->service->getLocalization();

    self::assertIsArray($localization);
  }

  /**
   * Tests user-friendly time conversion.
   *
   * @return void
   */
  public function testUserFriendlyTimeTaken(): void {
    self::assertEquals('', $this->service->userFriendlyTimeTaken(0));
    self::assertEquals('1m', $this->service->userFriendlyTimeTaken(59));
    self::assertEquals('2m', $this->service->userFriendlyTimeTaken(119));
    self::assertEquals('60m', $this->service->userFriendlyTimeTaken(3600));
    self::assertEquals('1h 5m', $this->service->userFriendlyTimeTaken(3899));
  }

  /**
   * Tests mastery level text generation.
   *
   * @return void
   */
  public function testMasteryLevelText(): void {
    self::assertEquals('Not finished yet.', $this->service->masteryLevelText(1, 0, 0));
    self::assertEquals('Not finished yet.', $this->service->masteryLevelText(1, 0, 1));
    self::assertEquals('Not finished yet.', $this->service->masteryLevelText(1, 1, 0));
    self::assertEquals('Maximum score on first attempt.', $this->service->masteryLevelText(1, 1, 1));
    self::assertEquals('Score above 51% with two or less attempts.', $this->service->masteryLevelText(3, 5, 1));
    self::assertEquals('Score above 51% with two or less attempts.', $this->service->masteryLevelText(3, 5, 2));
    self::assertEquals('Score below 50% or more than two attempts.', $this->service->masteryLevelText(3, 5, 3));
    self::assertEquals('Score below 50% or more than two attempts.', $this->service->masteryLevelText(1, 5, 1));
    self::assertEquals('Score below 50% or more than two attempts.', $this->service->masteryLevelText(1, 5, 2));
    self::assertEquals('Score below 50% or more than two attempts.', $this->service->masteryLevelText(1, 5, 3));
  }

  /**
   * Tests JS settings generation.
   *
   * @return void
   * @throws EntityStorageException
   */
  public function testGetJsSettings(): void {
    $localization = $this->service->getLocalization();
    $settings = $this->service->getJsSettings();

    self::assertEquals([
      'recaptchaSiteKey' => NULL,
      'l10n' => $localization,
      'language' => 'en',
    ], $settings);

    $user = $this->setUpCurrentUser(['uid' => 1]);
    $this->config
      ->set('recaptcha_site_key', 'public key')
      ->save();
    $settings = $this->service->getJsSettings();

    self::assertEquals([
      'recaptchaSiteKey' => 'public key',
      'l10n' => $localization,
      'language' => 'en',
      'user'=> [
        'display_name' => $user->getDisplayName(),
        'mail' => $user->getEmail(),
      ],
    ], $settings);
  }
}
