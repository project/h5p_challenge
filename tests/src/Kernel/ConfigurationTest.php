<?php

declare(strict_types=1);

namespace Drupal\Tests\h5p_challenge\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test description.
 *
 * @group h5p_challenge
 */
final class ConfigurationTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['h5p_challenge'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig('h5p_challenge');
  }

  /**
   * Tests initial configuration after install.
   *
   * @return void
   */
  public function testConfiguration(): void {
    $config = $this->config('h5p_challenge.config');
    self::assertNull($config->get('recaptcha_site_key'));
    self::assertNull($config->get('recaptcha_secret_key'));
    self::assertNull($config->get('email_from_address'));
  }

}
