<?php

declare(strict_types=1);

namespace Drupal\h5p_challenge_test;

use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;

/**
 * Returns preset of fake responses for outbound reCAPTCHA HTTP requests.
 */
final class CaptchaClientMiddleware {

  /**
   * HTTP middleware that returns fake HTTP responses for reCAPTCHA requests.
   */
  public function __invoke() {
    return function ($handler) {
      return function (RequestInterface $request, array $options) use ($handler) : PromiseInterface {
        if (str_contains((string) $request->getUri(), 'https://www.google.com/recaptcha/api/siteverify')) {
          // Only override $uri if it matches the reCAPTCHA service endpoint to avoid
          // changing any other uses of the 'http_client' service during tests with
          // this module installed.
          return $this->createPromise($request);
        }

        return $handler($request, $options);
      };
    };
  }

  /**
   * Creates a promise for the reCAPTCHA request.
   *
   * @param RequestInterface $request
   *
   * @return FulfilledPromise
   */
  protected function createPromise(RequestInterface $request): FulfilledPromise
  {
    $contentsRequest = (string) $request->getBody();

    if (str_contains($contentsRequest, 'response=500')) {
      $response = new Response(500);

      return new FulfilledPromise($response);
    }

    if (str_contains($contentsRequest, 'response=204')) {
      $response = new Response(204);

      return new FulfilledPromise($response);
    }

    $response = new Response(200, [], json_encode(['success' => TRUE]));

    return new FulfilledPromise($response);
  }

}
