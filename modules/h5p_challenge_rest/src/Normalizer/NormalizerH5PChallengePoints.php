<?php

namespace Drupal\h5p_challenge_rest\Normalizer;

use Drupal\h5p_challenge\FetchClass\H5PChallengePoints;
use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * StdClass normalizer.
 */
class NormalizerH5PChallengePoints extends NormalizerBase {

  /**
   * Constructs a NormalizerH5PChallengePoints object.
   *
   * @param string|array $supported_class
   *   The supported class(es).
   */
  public function __construct($supported_class) {
    // TODO This has to be removed as soon as we drop support for Drupal 9
    if (property_exists($this, 'supportedInterfaceOrClass')) {
      $this->supportedInterfaceOrClass = $supported_class;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($response, $format = NULL, array $context = []) {
    $attributes = [];

    foreach ($response as $name => $property) {
      if ($name === 'data') {
        $property = $property ? unserialize($property) : [];
      }

      if ($name === 'started' || $name === 'finished') {
        // TODO See if we need to set the timezone as the DateTimeNormalizer
        // does
        $property = DrupalDateTime::createFromTimestamp($property)
          ->format(\DateTime::RFC3339);
      }

      if (in_array($name, ['tries_count', 'user_id', 'points', 'max_points',])) {
        $property = (!is_null($property) && $property !== '') ? (int) $property : $property;
      }

      $attributes[$name] = $property;
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format) : array {
    return [
      H5PChallengePoints::class => TRUE,
    ];
  }

}
