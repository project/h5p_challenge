<?php

namespace Drupal\h5p_challenge_rest\Normalizer;

use Drupal\h5p_challenge\FetchClass\H5PChallenge;
use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * StdClass normalizer.
 */
class NormalizerH5PChallenge extends NormalizerBase {

  /**
   * Constructs a NormalizerH5PChallenge object.
   *
   * @param string|array $supported_class
   *   The supported class(es).
   */
  public function __construct($supported_class) {
    // TODO This has to be removed as soon as we drop support for Drupal 9
    if (property_exists($this, 'supportedInterfaceOrClass')) {
      $this->supportedInterfaceOrClass = $supported_class;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($challenge, $format = NULL, array $context = []) {
    $attributes = [];

    foreach ($challenge as $name => $property) {
      if ($name === 'data') {
        $property = $property ? unserialize($property) : [];
      }

      if ($name === 'started' || $name === 'finished') {
        // TODO See if we need to set the timezone as the DateTimeNormalizer
        // does
        $property = DrupalDateTime::createFromTimestamp($property)
          ->format(\DateTime::RFC3339);
      }

      if ($name === 'results_sent') {
        $property = (bool) $property;
      }

      if (in_array($name, ['content_id', 'user_id',])) {
        $property = (int) $property;
      }

      $attributes[$name] = $property;
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format) : array {
    return [
      H5PChallenge::class => TRUE,
    ];
  }

}
