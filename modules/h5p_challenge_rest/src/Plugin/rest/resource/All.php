<?php

namespace Drupal\h5p_challenge_rest\Plugin\rest\resource;

use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\h5p_challenge\FetchClass\H5PChallengePoints;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource to get all challenge related data.
 *
 * @RestResource(
 *   id = "h5p_challenge_all_resource",
 *   label = @Translation("All challege related data"),
 *   uri_paths = {
 *     "canonical" = "api/h5p_challenge/all"
 *   }
 * )
 */
class All extends ResourceBase {

  use FromAndUntilRestResourceTrait;
  use PaginationResourceTrait;

  /**
   * Database connection instance.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('h5p_challenge_rest');
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Responds to GET requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   */
  public function get(Request $request) {
    $this->request = $request;

    $this->validateFromAndUntil();

    $range_start = $this->getRangeStart();
    $range_length = $this->getLimit();

    $data = [
      'meta' => [
        'totalPages' => 0,
      ],
      'challenges' => [],
      'responses' => [],
    ];

    // Challenges
    $challenge_query = $this->database->select('h5p_challenge', 'c', [
      'fetch' => H5PChallenge::class,
    ]);
    $challenge_query->fields('c');
    $challenge_query->isNotNull('c.user_id');

    if ($this->hasQueryParam('user_id')) {
      $challenge_query->condition('c.user_id', $request->query->get('user_id'), '=');
    }
    if ($this->hasFromParam() && $from = $this->fromTimestamp()) {
      $challenge_query->condition('c.started', $from, '>=');
    }
    if ($this->hasUntilParam() && $until = $this->untilTimestamp()) {
      $challenge_query->condition('c.started', $until, '<=');
    }

    $count_challenge_query = clone $challenge_query;
    $challenge_count = $count_challenge_query->countQuery()->execute()->fetchField();

    $total_pages = (int) ceil($challenge_count / $range_length);

    $challenge_query->range($range_start, $range_length);
    $challenge_query->orderBy('c.started', 'ASC');

    $data['challenges'] = $challenge_query->execute()->fetchAll();

    // Responses
    $response_query = $this->database->select('h5p_challenge_points', 'cp', [
      'fetch' => H5PChallengePoints::class,
    ]);
    $response_query->innerJoin('h5p_challenge', 'c', 'c.uuid = cp.challenge_uuid');
    $response_query->isNotNull('c.user_id');

    if ($this->hasQueryParam('user_id')) {
      $response_query->condition('c.user_id', $request->query->get('user_id'), '=');
    }
    if ($this->hasFromParam() && $from = $this->fromTimestamp()) {
      $response_query->condition('cp.started', $from, '>=');
    }
    if ($this->hasUntilParam() && $until = $this->untilTimestamp()) {
      $response_query->condition('cp.started', $until, '<=');
    }

    $response_query->fields('cp');

    $count_response_query = clone $response_query;
    $response_count = $count_response_query->countQuery()->execute()->fetchField();

    if (ceil($response_count / $range_length) > $total_pages) {
      $total_pages = ceil($response_count / $range_length);
    }

    $response_query->range($range_start, $range_length);
    $response_query->orderBy('cp.started', 'ASC');

    $data['responses'] = $response_query->execute()->fetchAll();

    $data['meta']['totalPages'] = $total_pages;

    $data['links'] = $this->generateLinks($total_pages);

    $response = new ResourceResponse($data, 200);

    // TODO See if setting it non-cacheable is a better approach, see URL for details
    // https://drupal.stackexchange.com/a/224508/92770
    $response->addCacheableDependency($data);

    return $response;
  }

}
