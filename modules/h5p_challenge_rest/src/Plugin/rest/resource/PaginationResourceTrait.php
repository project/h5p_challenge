<?php

namespace Drupal\h5p_challenge_rest\Plugin\rest\resource;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\Core\Url;

trait PaginationResourceTrait {

  /**
   * HTTP Request
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Returns current page number with fallback to zero.
   *
   * @throws BadRequestHttpException
   *   If value is less than zero
   *
   * @return int
   *   Current page number
   */
  private function getPage() : int {
    $page = (int)$this->request->query->get('page');

    if ($page < 0) {
      throw new BadRequestHttpException('Malformed page parameter! Page value can not be less than zero.');
    }

    return $page;
  }

  /**
   * Returns default pagination limit.
   *
   * @return int
   *   Default pagination limit value
   */
  private function getDefaultLimit() : int {
    return 50;
  }

  /**
   * Returns limit value, defaulting to constant value.
   *
   * @throws BadRequestHttpException
   *   If limit is not berween 1 and 500
   *
   * @return int [description]
   */
  private function getLimit() : int {
    if (!$this->request->query->has('limit')) {
      return $this->getDefaultLimit();
    }

    $limit = (int)$this->request->query->get('limit');

    if (!($limit >= 1 && $limit <= 500)) {
      throw new BadRequestHttpException('Malformed limit parameter! Allowed limit values are from 1 to 500.');
    }

    return $limit;
  }

  /**
   * Determines range start value based on page and limit values.
   *
   * @return int
   *   Range start value
   */
  private function getRangeStart() : int {
    if ($this->request->query->has('page')) {
      return $this->getPage() * $this->getLimit();
    }

    return 0;
  }

  /**
   * Generated resource pagination links.
   *
   * @param  int   $total_pages
   *   Total pages value
   *
   * @return array
   *   Links structure
   */
  private function generateLinks(int $total_pages) : array {
    $links = [];
    $links['self'] = Url::fromUri($this->request->getUri(), [
      'absolute' => TRUE,
    ])->toString();
    if ($this->getPage() > 0) {
      $links['prev'] = Url::fromUri($this->request->getUri(), [
        'absolute' => TRUE,
        'query' => [
          'page' => $this->getPage() - 1,
        ],
      ])->toString();
    }
    if ($total_pages > 1 && $this->getPage() < ($total_pages - 1)) {
      $links['next'] = Url::fromUri($this->request->getUri(), [
        'absolute' => TRUE,
        'query' => [
          'page' => $this->getPage() + 1,
        ],
      ])->toString();
    }

    return $links;
  }

}
