<?php

namespace Drupal\h5p_challenge_rest\Plugin\rest\resource;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait FromAndUntilRestResourceTrait {

  /**
   * HTTP Request
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Checks if query string has a parameter present.
   *
   * @param  string  $param
   *   Param name.
   *
   * @return boolean
   *   Present in query string or not.
   */
  private function hasQueryParam(string $param) : bool {
    return $this->request->query->has($param);
  }

  /**
   * Checks if query string has a from parameter.
   *
   * @return boolean
   *   Present in query string or not.
   */
  private function hasFromParam() : bool {
    return $this->hasQueryParam('from');
  }

  /**
   * Checks if query string has an until parameter.
   *
   * @return boolean
   *   Present in query string or not.
   */
  private function hasUntilParam() : bool {
    return $this->hasQueryParam('until');
  }

  /**
   * Returns a timestamp created from request parameter using strtotime().
   *
   * @param  string  $param
   *   Param name.
   *
   * @return mixed
   *   Integer if conversion is successful, FALSE otherwise.
   */
  private function timestampFromQueryParam(string $param) {
    return strtotime($this->request->query->get($param));
  }

  /**
   * Returns timestamp based on from parameter.
   *
   * @return mixed
   *   Integer if conversion is successful, FALSE otherwise.
   */
  private function fromTimestamp() {
    return $this->timestampFromQueryParam('from');
  }

  /**
   * Returns timestamp based on until parameter.
   *
   * @return mixed
   *   Integer if conversion is successful, FALSE otherwise.
   */
  private function untilTimestamp() {
    return $this->timestampFromQueryParam('until');
  }

  /**
   * Validates from and until parameters and throws BadRequestHttpException if
   * any is present but can not be converted to timestamp or from timestamp
   * value is less than until.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   *   Throws exception expected.
   */
  private function validateFromAndUntil() : void {
    if ($this->hasFromParam()) {
      $from = $this->fromTimestamp();

      if ($from === FALSE) {
        throw new BadRequestHttpException('Malformed from parameter!');
      }
    }

    if ($this->hasUntilParam()) {
      $until = $this->untilTimestamp();

      if ($until === FALSE) {
        throw new BadRequestHttpException('Malformed until parameter!');
      }
    }

    if (isset($from) && isset($until) && $from && $until) {
      if ($from > $until) {
        throw new BadRequestHttpException('From parameter value is less than until!');
      }
    }
  }

}
