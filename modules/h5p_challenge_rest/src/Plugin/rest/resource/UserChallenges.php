<?php

namespace Drupal\h5p_challenge_rest\Plugin\rest\resource;

use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\h5p_challenge\FetchClass\H5PChallenge;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "h5p_challenge_user_challenges_resource",
 *   label = @Translation("User challenges"),
 *   uri_paths = {
 *     "canonical" = "api/h5p_challenge/user/{id}/challenges"
 *   }
 * )
 */
class UserChallenges extends ResourceBase {

  use FromAndUntilRestResourceTrait;
  use PaginationResourceTrait;

  /**
   * Database connection instance.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('h5p_challenge_rest');
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * Responds to GET requests.
   *
   * @param int $id
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   */
   public function get(int $id, Request $request) {
     $this->request = $request;

     $this->validateFromAndUntil();

     $range_start = $this->getRangeStart();
     $range_length = $this->getLimit();

     $data = [
       'meta' => [
         'totalPages' => 0,
       ],
       'challenges' => [],
     ];

     // TODO Enable from, until and pagination
     $query = $this->database->select('h5p_challenge', 'c', [
       'fetch' => H5PChallenge::class,
     ]);

     if ($this->hasFromParam() && $from = $this->fromTimestamp()) {
       $query->condition('c.started', $from, '>=');
     }
     if ($this->hasUntilParam() && $until = $this->untilTimestamp()) {
       $query->condition('c.started', $until, '<=');
     }

     $query->fields('c');
     $query->condition('c.user_id', $id, '=');

     $count_query = clone $query;
     $count = $count_query->countQuery()->execute()->fetchField();

     $total_pages = (int) ceil($count / $range_length);

     $query->range($range_start, $range_length);
     $query->orderBy('c.started', 'ASC');

     $data['challenges'] = $query->execute()->fetchAll();

     $data['meta']['totalPages'] = $total_pages;

     $data['links'] = $this->generateLinks($total_pages);

     $response = new ResourceResponse($data, 200);

     // TODO See if setting it non-cacheable is a better approach, see URL for details
     // https://drupal.stackexchange.com/a/224508/92770
     $response->addCacheableDependency($data);

     return $response;
   }

}
